写在前面的感谢，此软件虽然是本人独立开发，但是参考以下各位大神的作品：  
1、老版的D2Loader.exe  
2、原版的game.exe  
3、D2SE  
4、Necrolis发在https://d2mods.info/forum/viewtopic.php?t=62807的精华帖子  
5、d2mods.info的诸多大神的帖子  
6、https://github.com/jankowskib/d2console.git  
7、特别鸣谢：CaiMiao大佬对软件DPI等方面提出的改善意见  
8、还有许许多多无名大佬的工作，都是我完成这个软件的基础，这里无法一一列举了，全都表示衷心的感谢和佩服！  如有冒犯，可以联系我。
  
本项目完全由我本人开发完成，兼容v1.09d/1.10f/1.11b/1.12a/1.13c/1.13d，可以多开，可以联网。  
默认根据同目录下的game.exe来判断版本号，你也可以自行修改，特点如下：  
  
建议仔细阅读《D2ModCenter使用手册.doc》。  
  
1、一键安装、一键卸载，无需修改storm.dll，直接用原版。  
2、支持原版game.exe的所有命令行参数，比如-w、-direct、-txt、-glide、-3dfx、-locale、-title等。特别说明，常用参数可以放到同目录的D2Loader.ini里，这样就不用再写批处理或者建快捷方式了。  
3、兼容v1.09d/1.10f/1.11b/1.12a/1.13c/1.13d。  
4、默认根据同目录下的game.exe来判断版本号，也可以用参数-ver 1.13c来指定游戏版本号。  
5、支持单人、TCP/IP、战网。  
6、常用参数说明：  
-ver 指定游戏版本号，比如-ver 1.13d  
-xp 自动设置XP兼容，不需要在exe上右键设置了  
-console 开启console窗口，看到输出信息  
-hack 类似d2gs的动态patch，其中d2hack.script文件名可以任意替换成自己的，具体的文件格式可以参考Install目录下的例子，已经实现了1.13c的一拳满级和一元购物  
-hackpre 这个参数指定的script会在其他插件之前加载，而-hack是在其他插件之后加载，适配多种情况  
-glide/-3dfx 3dfx模式  
-w -direct -txt 不用介绍了  
-title 设定游戏窗口标题  
-notitle 游戏窗口不显示任何标题  
-ns -nosound 无声模式  
-res800 -res640 设定游戏分辨率  
-locale 比如-locale CHI，则自动加载Language_CHI\\CHI.mpq  
-mpq 指定额外的mpq，最多10个，空格分开，可以带路径，路径有空格要用双引号，比如：-mpq Language_CHI\CHI.mpq  
-plugin 指定额外的dll，最多10个，空格分开，可以带路径，路径有空格要用双引号，比如：-plugin d2hackmap\d2hackmap.dll。另外，可以追加plugin的初始化函数，比如：-plugin PlugY.dll:_Init@4  
-mpqpath 指定10 mpqs的加载路径，路径有空格要用双引号，路径的最后不要写反斜杠"\\"，比如：-mpqpath "d:\Diablo II D2SE"  
-dllpath 追加dll的加载路径，路径有空格要用双引号，路径的最后不要写反斜杠"\\"，比如：-dllpath "d:\Diablo II D2SE" "d:\Diablo II D2SE\D2SE\CORES\1.13c"  
-depfix 跳过战网版本检查，去除dep机制  
-noborder 窗口无边框  
-multiopen 多开  
-modpath 自动替代注册表里的InstallPath和Save Path参数，重定向到modpath下面  
-nodide 窗口不失去焦点，用于一键跟随，需要打开hackmap的Out Town Select Toggle，否则城外无法选中其他角色。使用方法：  
1.人物A队长，出城。  
2.人物B出城，左键点击人物A，鼠标别动，点住人物A，拖动鼠标到游戏窗口外。  
3.鼠标别回到跟随人物窗口操作。  
4.鼠标转移到人物A游戏窗口，操作人物A，其他人物自动跟随成功。  
备注：普通攻击模式点击  
-nosleep  
-opengl/-d3d/-per/-lq/-gamma/-vsync/-fr/-joinid/-gamename/-bn/-mcpip/-nopk/-openc/-arena/-txt/-direct/-ama/-pal/-sor/-nec/-bar/-dru/-asn/-i/-bnacct/-bnpass/-name/-realm/-ctemp/-nm/-m/-minfo/-md/-unique/-act/-log/-msglog/-safe/-seed/-cheats/-ns/-nosound/-questall/-npl/-lem/-nocompress/-gamepass/-skiptobnet/-client/-server/-launch/-title/-notitle/-res800/-res640 都是原版参数，代码里有部分说明，自己摸索吧