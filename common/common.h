﻿#ifndef COMMON_H
#define COMMON_H

enum eGameVersion
{
    UNKNOWN = -1,
    V100 = 0,
    V107,
    V108,
    V109,
    V109b,
    V109d,
    V110f,
    V111,
    V111b,
    V112a,
    V113c,
    V113d,
    V114a,
    V114b,
    V114c,
    V114d
};

const char* GetVersionString(int version);
eGameVersion GetD2Version(const char* gameExe);
int msgBox(LPCSTR boxName, UINT uType, LPCSTR pFormat, ...);
int D2Loader_GetVersionId(char *pcVersion);

#endif
