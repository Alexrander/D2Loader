﻿
// D2ModCenterDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "D2ModCenter.h"
#include "D2ModCenterDlg.h"
#include "afxdialogex.h"
#include <direct.h>
#include <locale>
#include "tlhelp32.h"

#pragma comment(lib, "Version.Lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define D2TRUE TRUE
#define D2FALSE FALSE

static CString m_strMyVersion = CString("D2ModCenter ") + CString(D2LOADER_VERSION_STR);
static BOOL m_bRestartFlag = FALSE;
static BOOL m_boolTest = FALSE;
static DWORD m_dwCurrentLocale = 1;
static TCHAR m_acWorkingPath[D2LOADER_MAXPATH] = {0};
static CString m_strDefaultTip;
#define MAX_PROMPT_INFO 30
static DWORD m_dwCurrentPrompt = 0;
static CString m_astrPrompt[MAX_PROMPT_INFO];

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
    enum { IDD = IDD_ABOUTBOX };
#endif

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
    // 生成的消息映射函数
    virtual BOOL OnInitDialog();

    DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedChecktest();
    CComboBox m_cbSoftLocale;
    afx_msg void OnCbnSelchangeSoftwarelocale();
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

BOOL CAboutDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    SetDlgItemText(IDC_STATICVERSION, m_strMyVersion);

    if (TRUE == m_boolTest)
    {
        ((CButton*)GetDlgItem(IDC_CHECKTEST))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKTEST))->SetCheck(0);
    }

    m_cbSoftLocale.ResetContent();
    m_cbSoftLocale.AddString(_T("English"));
    m_cbSoftLocale.AddString(_T("简体中文"));
    m_cbSoftLocale.SetCurSel(m_dwCurrentLocale);

    return TRUE;
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_SOFTWARELOCALE, m_cbSoftLocale);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
    ON_BN_CLICKED(IDC_CHECKTEST, &CAboutDlg::OnBnClickedChecktest)
    ON_CBN_SELCHANGE(IDC_SOFTWARELOCALE, &CAboutDlg::OnCbnSelchangeSoftwarelocale)
END_MESSAGE_MAP()


// CD2ModCenterDlg 对话框

void CD2ModCenterDlg::resetClientData()
{
    memset(&m_stModParam, 0, sizeof(m_stModParam));
}

CD2ModCenterDlg::CD2ModCenterDlg(CWnd* pParent /*=nullptr*/)
    : CDialogEx(IDD_D2MODCENTER_DIALOG, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

    m_dwModCount = 0;
    m_iModIndex = -1;
    m_ModList.DeleteAllItems();
    m_boolWaitGameFinish = FALSE;

    resetClientData();
}

void CD2ModCenterDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_LISTMOD, m_ModList);
    DDX_Control(pDX, IDC_COMBO_VER, m_cbVersion);
    DDX_Control(pDX, IDC_COMBO_VIDEO, m_cbVideoMode);
    DDX_Control(pDX, IDC_COMBO_LOCALE, m_cbLocalePath);
}

BEGIN_MESSAGE_MAP(CD2ModCenterDlg, CDialogEx)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_NOTIFY(NM_CLICK, IDC_LISTMOD, &CD2ModCenterDlg::OnNMClickListmod)
    ON_NOTIFY(NM_CUSTOMDRAW, IDC_LISTMOD, &CD2ModCenterDlg::OnNMCustomdrawListmod)
    ON_CBN_SELCHANGE(IDC_COMBO_VER, &CD2ModCenterDlg::OnCbnSelchangeComboVer)
    ON_CBN_SELCHANGE(IDC_COMBO_VIDEO, &CD2ModCenterDlg::OnCbnSelchangeComboVideo)
    ON_CBN_SELCHANGE(IDC_COMBO_LOCALE, &CD2ModCenterDlg::OnCbnSelchangeComboLocale)
    ON_BN_CLICKED(IDC_CHECKWINDOW, &CD2ModCenterDlg::OnBnClickedCheckwindow)
    ON_EN_CHANGE(IDC_HACKPRE, &CD2ModCenterDlg::OnEnChangeHackpre)
    ON_EN_CHANGE(IDC_HACK, &CD2ModCenterDlg::OnEnChangeHack)
    ON_EN_CHANGE(IDC_TITLE, &CD2ModCenterDlg::OnEnChangeTitle)
    ON_BN_CLICKED(IDC_CHECKMULTIOPEN, &CD2ModCenterDlg::OnBnClickedCheckmultiopen)
    ON_BN_CLICKED(IDC_CHECKXP, &CD2ModCenterDlg::OnBnClickedCheckxp)
    ON_BN_CLICKED(IDC_CHECKDEPFIX, &CD2ModCenterDlg::OnBnClickedCheckdepfix)
    ON_BN_CLICKED(IDC_CHECKDIRECT, &CD2ModCenterDlg::OnBnClickedCheckdirect)
    ON_BN_CLICKED(IDC_CHECKTXT, &CD2ModCenterDlg::OnBnClickedChecktxt)
    ON_BN_CLICKED(IDC_CHECKNOSOUND, &CD2ModCenterDlg::OnBnClickedChecknosound)
    ON_BN_CLICKED(IDC_CHECKNOTITLE, &CD2ModCenterDlg::OnBnClickedChecknotitle)
    ON_BN_CLICKED(IDC_CHECKNOBORDER, &CD2ModCenterDlg::OnBnClickedChecknoborder)
    ON_BN_CLICKED(IDC_CHECKSKIPTOBNET, &CD2ModCenterDlg::OnBnClickedCheckskiptobnet)
    ON_BN_CLICKED(IDC_CHECKNOHIDE, &CD2ModCenterDlg::OnBnClickedChecknohide)
    ON_BN_CLICKED(IDC_CHECKNOSLEEP, &CD2ModCenterDlg::OnBnClickedChecknosleep)
    ON_BN_CLICKED(IDC_CHECKCONSOLE, &CD2ModCenterDlg::OnBnClickedCheckconsole)
    ON_BN_CLICKED(IDC_CHECKMPQPATH, &CD2ModCenterDlg::OnBnClickedCheckmpqpath)
    ON_BN_CLICKED(IDC_CHECKMPQFILE, &CD2ModCenterDlg::OnBnClickedCheckmpqfile)
    ON_BN_CLICKED(IDC_CHECKDLLPATH, &CD2ModCenterDlg::OnBnClickedCheckdllpath)
    ON_BN_CLICKED(IDC_CHECKDLLFILE, &CD2ModCenterDlg::OnBnClickedCheckdllfile)
    ON_BN_CLICKED(IDC_CHECKPLUGY, &CD2ModCenterDlg::OnBnClickedCheckplugy)
    ON_BN_CLICKED(IDC_BTNDELMPQPATH, &CD2ModCenterDlg::OnBnClickedBtndelmpqpath)
    ON_BN_CLICKED(IDC_BTNDELDLLPATH, &CD2ModCenterDlg::OnBnClickedBtndeldllpath)
    ON_BN_CLICKED(IDC_BTNDELMPQFILE, &CD2ModCenterDlg::OnBnClickedBtndelmpqfile)
    ON_BN_CLICKED(IDC_BTNDELDLLFILE, &CD2ModCenterDlg::OnBnClickedBtndeldllfile)
    ON_BN_CLICKED(IDC_BTNMPQPATH, &CD2ModCenterDlg::OnBnClickedBtnmpqpath)
    ON_BN_CLICKED(IDC_BTNDLLPATH, &CD2ModCenterDlg::OnBnClickedBtndllpath)
    ON_BN_CLICKED(IDC_BTNMPQFILE, &CD2ModCenterDlg::OnBnClickedBtnmpqfile)
    ON_BN_CLICKED(IDC_BTNDLLFILE, &CD2ModCenterDlg::OnBnClickedBtndllfile)
    ON_BN_CLICKED(IDC_BTNHELP, &CD2ModCenterDlg::OnBnClickedBtnhelp)
    ON_BN_CLICKED(IDC_BTNSTART, &CD2ModCenterDlg::OnBnClickedBtnstart)
    ON_NOTIFY(NM_RCLICK, IDC_LISTMOD, &CD2ModCenterDlg::OnNMRClickListmod)
    ON_COMMAND(ID_CLISTMENU_32771, &CD2ModCenterDlg::OnClistmenu32771)
    ON_BN_CLICKED(IDC_BTND2VIDTST, &CD2ModCenterDlg::OnBnClickedBtnd2vidtst)
    ON_BN_CLICKED(IDC_CHECKCOPYD2HD, &CD2ModCenterDlg::OnBnClickedCheckcopyd2hd)
    ON_BN_CLICKED(IDC_BTNHACKPRE, &CD2ModCenterDlg::OnBnClickedBtnhackpre)
    ON_BN_CLICKED(IDC_BTNHACK, &CD2ModCenterDlg::OnBnClickedBtnhack)
    ON_BN_CLICKED(IDC_BTNKILLD2, &CD2ModCenterDlg::OnBnClickedBtnkilld2)
    ON_BN_CLICKED(IDC_BTNBASEMOD, &CD2ModCenterDlg::OnBnClickedBtnbasemod)
    ON_WM_TIMER()
    ON_WM_CLOSE()
    ON_BN_CLICKED(IDC_CHECKRESTOREDLL, &CD2ModCenterDlg::OnBnClickedCheckrestoredll)
    ON_BN_CLICKED(IDC_CHECKCOMMONDLL, &CD2ModCenterDlg::OnBnClickedCheckcommondll)
    ON_BN_CLICKED(IDC_CHECKALLDLL, &CD2ModCenterDlg::OnBnClickedCheckalldll)
END_MESSAGE_MAP()

CStringA CD2ModCenterDlg::ReadD2LoaderIni(CString filename)
{
    CString strReadData = _T("");
    CStringA strRetData;
    CStdioFile csFile;
    CFileException cfException;

    if (csFile.Open(filename, CFile::typeText | CFile::modeRead | CFile::modeNoTruncate, &cfException))//以txt方式读取|文件打开时不清除
    {
        char* old_locale = _strdup( setlocale(LC_CTYPE,NULL) );
        setlocale( LC_CTYPE, "chs" );//设定 
        csFile.ReadString(strReadData);
        setlocale( LC_CTYPE, old_locale );
        free( old_locale );//还原区域设定 
        csFile.Close();
    }

    strReadData.Replace(_T("..\\.."), m_strWorkingPath);
    strRetData = strReadData;
    return strRetData;
}

void CD2ModCenterDlg::parse_command(int argc, char** argv, ST_CLIENT_DATA *pstClientData)
{
    DWORD j;

    resetClientData();

    for ( int i = 1; i < argc; ++i )
    {
        if ( !_stricmp("-ver", argv[i]) )
        {
            i++;
            m_strVersion = argv[i];
        }
        else if ( !_stricmp("-nohide", argv[i]) )
        {
            m_stModParam.boolNoHide = TRUE;
        }
        else if ( !_stricmp("-nosleep", argv[i]) )
        {
            m_stModParam.boolNoSleep = TRUE;
        }
        else if ( !_stricmp("-xp", argv[i]) )
        {
            m_stModParam.boolXpCompatible = TRUE;
        }
        else if ( !_stricmp("-console", argv[i]) )
        {
            m_stModParam.boolWithConsole = TRUE;
        }
        else if ( !_stricmp("-hackpre", argv[i]) )
        {
            i++;
            strncpy_s(m_stModParam.acHackScriptPre, argv[i], sizeof(m_stModParam.acHackScriptPre) - 1);
        }
        else if ( !_stricmp("-hack", argv[i]) )
        {
            i++;
            strncpy_s(m_stModParam.acHackScript, argv[i], sizeof(m_stModParam.acHackScript) - 1);
        }
        else if ( !_stricmp("-w", argv[i]) )
        {
            pstClientData->window_mode = D2TRUE;
        }
        else if ( !_stricmp("-glide", argv[i]) || !_stricmp("-3dfx", argv[i]) )
        {
            pstClientData->glide_mode = D2TRUE;
        }
        else if ( !_stricmp("-opengl", argv[i]) )
        {
            pstClientData->opengl_mode = D2TRUE;
        }
        else if ( !_stricmp("-d3d", argv[i]) )
        {
            pstClientData->d3d_mode = D2TRUE;
        }
        else if ( !_stricmp("-per", argv[i]) )
        {
            pstClientData->perspective = D2TRUE;
        }
        else if ( !_stricmp("-lq", argv[i]) )
        {
            pstClientData->low_quality = D2TRUE;
        }
        else if ( !_stricmp("-gamma", argv[i]) && i + 1 < argc )
        {
            i++;
            pstClientData->gamma = atoi(argv[i]);
        }
        else if ( !_stricmp("-vsync", argv[i]) )
        {
            pstClientData->vsync = D2TRUE;
        }
        else if ( !_stricmp("-fr", argv[i]) && i + 1 < argc )
        {
            i++;
            pstClientData->frame_rate = atoi(argv[i]);
        }
        else if ( !_stricmp("-joinid", argv[i]) && i + 1 < argc )
        {
            i++;
            pstClientData->join_id = atoi(argv[i]);
        }
        else if ( !_stricmp("-gamename", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(pstClientData->game_name, argv[i], sizeof(pstClientData->game_name) - 1);
        }
        else if ( !_stricmp("-bn", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(pstClientData->bnet_ip, argv[i], sizeof(pstClientData->bnet_ip) - 1);
        }
        else if ( !_stricmp("-mcpip", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(pstClientData->mcp_ip, argv[i], sizeof(pstClientData->mcp_ip) - 1);
        }
        else if ( !_stricmp("-nopk", argv[i]) )
        {
            pstClientData->no_pk = D2TRUE;
        }
        else if ( !_stricmp("-openc", argv[i]) )
        {
            pstClientData->open_c = D2TRUE;
        }
        else if ( !_stricmp("-arena", argv[i]) )
        {
            pstClientData->arena = D2TRUE;
        }
        else if ( !_stricmp("-txt", argv[i]) )
        {
            pstClientData->txt = D2TRUE;
        }
        else if ( !_stricmp("-ama", argv[i]) )
        {
            pstClientData->amazon = D2TRUE;
        }
        else if ( !_stricmp("-pal", argv[i]) )
        {
            pstClientData->paladin = D2TRUE;
        }
        else if ( !_stricmp("-sor", argv[i]) )
        {
            pstClientData->sorceress = D2TRUE;
        }
        else if ( !_stricmp("-nec", argv[i]) )
        {
            pstClientData->necromancer = D2TRUE;
        }
        else if ( !_stricmp("-bar", argv[i]) )
        {
            pstClientData->barbarian = D2TRUE;
        }
        else if ( !_stricmp("-dru", argv[i]) )
        {
            pstClientData->dru = D2TRUE;
        }
        else if ( !_stricmp("-asn", argv[i]) )
        {
            pstClientData->asn = D2TRUE;
        }
        else if ( !_stricmp("-i", argv[i]) )
        {
            pstClientData->invincible = D2TRUE;
        }
        else if ( !_stricmp("-bnacct", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(pstClientData->account_name, argv[i], sizeof(pstClientData->account_name) - 1);
        }
        else if ( !_stricmp("-bnpass", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(pstClientData->game_pass, argv[i], sizeof(pstClientData->game_pass) - 1);
        }
        else if ( !_stricmp("-name", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(pstClientData->player_name, argv[i], sizeof(pstClientData->player_name) - 1);
        }
        else if ( !_stricmp("-realm", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(pstClientData->realm_name, argv[i], sizeof(pstClientData->realm_name) - 1);
        }
        else if ( !_stricmp("-ctemp", argv[i]) && i + 1 < argc )
        {
            i++;
            pstClientData->c_temp = atoi(argv[i]);
        }
        else if ( !_stricmp("-nm", argv[i]) )
        {
            pstClientData->no_monsters = D2TRUE;
        }
        else if ( !_stricmp("-m", argv[i]) )
        {
            pstClientData->monster_class = D2TRUE;
        }
        else if ( !_stricmp("-minfo", argv[i]) )
        {
            pstClientData->monster_info = D2TRUE;
        }
        else if ( !_stricmp("-md", argv[i]) )
        {
            pstClientData->monster_debug = D2TRUE;
        }
        else if ( !_stricmp("-rare", argv[i]) )
        {
            pstClientData->item_rare = D2TRUE;
        }
        else if ( !_stricmp("-unique", argv[i]) )
        {
            pstClientData->item_unique = D2TRUE;
        }
        else if ( !_stricmp("-act", argv[i]) && i + 1 < argc )
        {
            i++;
            pstClientData->act = atoi(argv[i]);
        }
        else if ( !_stricmp("-log", argv[i]) )
        {
            pstClientData->log = D2TRUE;
        }
        else if ( !_stricmp("-msglog", argv[i]) )
        {
            pstClientData->msg_log = D2TRUE;
        }
        else if ( !_stricmp("-safe", argv[i]) )
        {
            pstClientData->safe_mode = D2TRUE;
        }
        else if ( !_stricmp("-seed", argv[i]) && i + 1 < argc )
        {
            i++;
            pstClientData->seed = atoi(argv[i]);
        }
        else if ( !_stricmp("-cheats", argv[i]) )
        {
            pstClientData->cheats = D2TRUE;
        }
        else if ( !_stricmp("-ns", argv[i]) || !_stricmp("-nosound", argv[i]) )
        {
            pstClientData->no_sound = D2TRUE;
        }
        else if ( !_stricmp("-questall", argv[i]) )
        {
            pstClientData->quests = D2TRUE;
        }
        else if ( !_stricmp("-npl", argv[i]) )
        {
            pstClientData->no_preload = D2TRUE;
        }
        else if ( !_stricmp("-direct", argv[i]) )
        {
            pstClientData->direct = D2TRUE;
        }
        else if ( !_stricmp("-lem", argv[i]) )
        {
            pstClientData->low_end = D2TRUE;
        }
        else if ( !_stricmp("-nocompress", argv[i]) )
        {
            pstClientData->no_gfx_compress = D2TRUE;
        }
        else if ( !_stricmp("-gamepass", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(pstClientData->game_pass, argv[i], sizeof(pstClientData->game_pass) - 1);
        }
        else if ( !_stricmp("-skiptobnet", argv[i]) )
        {
            pstClientData->skip_to_bnet = D2TRUE;
        }
        else if ( !_stricmp("-client", argv[i]) )
        {
        }
        else if ( !_stricmp("-server", argv[i]) )
        {
        }
        else if ( !_stricmp("-launch", argv[i]) )
        {
        }
        else if ( !_stricmp("-notitle", argv[i]) )
        {
            strncpy_s(m_stModParam.acGameTitle, "notitle", sizeof(m_stModParam.acGameTitle) - 1);
        }
        else if ( !_stricmp("-title", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(m_stModParam.acGameTitle, argv[i], sizeof(m_stModParam.acGameTitle) - 1);
        }
        else if ( !_stricmp("-locale", argv[i]) && i + 1 < argc )
        {
            sprintf_s(m_stModParam.acLanguageMpq, "Language_%s\\%s.mpq", argv[i+1], argv[i+1]);
            ++i;
        }
        else if ( !_stricmp("-mpq", argv[i]) )
        {
            //指定额外的mpq，最多10个，空格分开，可以带路径，路径有空格要用双引号，比如：-mpq Language_CHI\CHI.mpq
            while ( i + 1 < argc && '-' != argv[i+1][0] && m_stModParam.dwExtendMpq < MAX_EXTEND_MPQ )
            {
                i++;
                sprintf_s(m_stModParam.aacExtendMpq[m_stModParam.dwExtendMpq], "%s", argv[i]);
                m_stModParam.dwExtendMpq++;
            }
        }
        else if ( !_stricmp("-plugin", argv[i]) )
        {
            //指定额外的dll，最多10个，空格分开，可以带路径，路径有空格要用双引号，比如：-plugin d2hackmap\d2hackmap.dll
            //另外，可以追加plugin的初始化函数，比如：-plugin PlugY.dll:_Init@4
            while ( i + 1 < argc && '-' != argv[i+1][0] && m_stModParam.dwExtendPlugin < MAX_EXTEND_PLUGIN )
            {
                i++;
                sprintf_s(m_stModParam.aacExtendPlugin[m_stModParam.dwExtendPlugin], "%s", argv[i]);
                m_stModParam.dwExtendPlugin++;
            }
        }
        else if ( !_stricmp("-mpqpath", argv[i]) && i + 1 < argc )
        {
            //追加mpqs的加载路径，路径有空格要用双引号，路径的最后不要写反斜杠"\\"，比如：-mpqpath "d:\Diablo II D2SE"
            while ( i + 1 < argc && '-' != argv[i+1][0] && m_stModParam.dwGlobalMpqPath < MAX_EXTEND_MPQ_PATH )
            {
                i++;
                sprintf_s(m_stModParam.aacGlobalMpqPath[m_stModParam.dwGlobalMpqPath], "%s", argv[i]);
                j = strlen(m_stModParam.aacGlobalMpqPath[m_stModParam.dwGlobalMpqPath]) - 1;
                while ( '\\' == m_stModParam.aacGlobalMpqPath[m_stModParam.dwGlobalMpqPath][j] )
                {
                    m_stModParam.aacGlobalMpqPath[m_stModParam.dwGlobalMpqPath][j] = 0;
                    j--;
                }
                m_stModParam.dwGlobalMpqPath++;
            }
        }
        else if ( !_stricmp("-dllpath", argv[i]) )
        {
            //追加dll的加载路径，路径有空格要用双引号，路径的最后不要写反斜杠"\\"，比如：-dllpath "d:\Diablo II D2SE" "d:\Diablo II D2SE\D2SE\CORES\1.13c"
            while ( i + 1 < argc && '-' != argv[i+1][0] && m_stModParam.dwGlobalDllPath < MAX_EXTEND_PLUGIN_PATH )
            {
                i++;
                sprintf_s(m_stModParam.aacGlobalDllPath[m_stModParam.dwGlobalDllPath], "%s", argv[i]);
                j = strlen(m_stModParam.aacGlobalDllPath[m_stModParam.dwGlobalDllPath]) - 1;
                while ( '\\' == m_stModParam.aacGlobalDllPath[m_stModParam.dwGlobalDllPath][j] )
                {
                    m_stModParam.aacGlobalDllPath[m_stModParam.dwGlobalDllPath][j] = 0;
                    j--;
                }
                m_stModParam.dwGlobalDllPath++;
            }
        }
        else if ( !_stricmp("-depfix", argv[i]) )
        {
            m_stModParam.boolDepFix = TRUE;
        }
        else if ( !_stricmp("-noborder", argv[i]) )
        {
            m_stModParam.boolNoBorder = TRUE;
        }
        else if ( !_stricmp("-multiopen", argv[i]) )
        {
            m_stModParam.boolMultiOpen = TRUE;
        }
        else if (!_stricmp("-modpath", argv[i]) && i + 1 < argc)
        {
            i++;
            strncpy_s(m_stModParam.acModPath, argv[i], sizeof(m_stModParam.acModPath) - 1);
        }
        else if ( !_stricmp("-ddraw", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(m_stModParam.acDDrawPlugin, argv[i], sizeof(m_stModParam.acDDrawPlugin) - 1);
        }
        else if ( !_stricmp("-d2dx", argv[i]) && i + 1 < argc )
        {
            i++;
            strncpy_s(m_stModParam.acD2DxPlugin, argv[i], sizeof(m_stModParam.acD2DxPlugin) - 1);
        }
        else if ( !_stricmp("-takeplugin", argv[i]) )
        {
            m_stModParam.boolTakePlugin = TRUE;
        }
        else if ( !_stricmp("-d2common", argv[i]) )
        {
            m_stModParam.boolD2Common = TRUE;
        }
        else if ( !_stricmp("-showalldll", argv[i]) )
        {
            m_stModParam.boolShowAllDll = TRUE;
        }
    }
    /*
    举两个例子：
    D2SE版1.13c
    D2Loader.exe -w -mpq language_chi\chi.mpq -mpqpath "d:\Diablo II D2SE" -dllpath "d:\Diablo II D2SE" "d:\Diablo II D2SE\D2se\CORES\1.13c" -plugin PlugY.dll:_Init@4 -glide -direct -txt
    或者
    D2Loader.exe -w -locale chi -mpqpath "d:\Diablo II D2SE" -dllpath "d:\Diablo II D2SE" "d:\Diablo II D2SE\D2se\CORES\1.13c" -plugin PlugY.dll:_Init@4 -glide -direct -txt
    D2SE版1.13d
    D2Loader.exe -w -mpqpath "d:\Diablo II D2SE" -direct -txt -plugin PlugY.dll:_Init@4 -glide
    */
    /*
    1.视频选项
    -exp -expansion 切换到扩展模式
    -w -window 切换到窗口模式
    -glide -glide 使用Glide显示模式
    -opengl -opengl 使用OpenGL显示模式
    -d3d -d3d 使用Direct 3D显示模式
    -rave -rave 使用Rave显示模式，仅适用于Mac
    -per -perspective 打开透视模式，仅适用于全屏非Direct Draw模式
    -lq -lowquality 低图像质量(高速度)
    -gamma -gamma 设置Gamma值为
    -vsync -vsync 打开VSync
    -fr -framerate 甚至帧速率为
    2.网络选项
    -s -serverip 设置TCP/IP游戏服务器的IP为
    -gametype -gametype 设置游戏类型为
    -joinid -joinid 设置加入游戏的ID为
    -gamename -gamename 设置游戏名为
    -bn -battlenetip 设置battle.net服务器IP为
    -mcpip -mcpip 设置mcpip服务器IP为
    -nopk -nopk 禁止PK(好像无效)
    -openc -openc 不清楚
    3.游戏选项
    -arena -arena 无效
    -difficulty -difficulty 无效
    -txt -txt 给MOD制作者，用于创建.bin文件
    4.角色选项
    -ama -ama 设置角色类型为ama
    -pal -pal 设置角色类型为pal
    -sor -sor 设置角色类型为sor
    -nec -nec 设置角色类型为nec
    -bar -bar 设置角色类型为bar
    -dru -dru 设置角色类型为dru
    -asn -asn 设置角色类型为asn
    -i -invincible 隐形？(好像无效)
    -bnacct -bnacct 设置battle.net账号名字为
    -bnpass -bnpass 设置battle.net密码为
    -name -name 设置battle.net角色名字为
    -realm -realm 设置battle.net服务器(Realm)名字为
    -ctemp -ctemp 在arena模式使用第个角色的模板
    5.怪物选项
    -nm -nomonster 无怪物？(无效)
    -m -monsterclass 不清楚
    -minfo -monsterinfo 显示怪物信息？(无效)
    -md -monsterdebug 不清楚
    6.物品选项
    -rare -rare 全稀有( Rare )物品？(无效)
    -unique -unique 全独特(Unique)物品？(无效)
    7.界面选项
    -act -act 设置初始位置为第幕
    8.Debug选项
    -log -log 激活log(无效)
    -msglog -msglog 激活msglog
    -safe -safemode 安全模式？
    -seed -seed 设置地图种子(ala5:可理解为地图ID)为
    -cheats -cheats 不清初
    -ns -nosound 无声模式
    -questall -questall 不清楚
    9.文件输入输出选项
    -npl -nopreload 不预读取游戏文件
    -direct -direct 直接从硬盘上(ala5:而非mpq文件中)读取数据
    -lem -lowend 不清楚
    -nocompress -nocompress 无压缩
    -comint -comint 动态数据结构(别碰它)
    -token -token 设置关闭游戏的令牌为
    -gamepass -gamepass 设置游戏密码为
    -skiptobnet -skiptobnet 直接进入battle.net
    10.定制选项
    -client -client 客户端模式
    -server -server 服务器端模式，需要d2server.dll
    -launch -launch 运行模式(默认)
    -notitle -notitle 无窗口标题栏
    -res800 -res800 窗口大小为800x600(仅适用于D2，对D2X无效)
    -res640 -res640 窗口大小为640x480(仅适用于D2，对D2X无效)
    -nonotify -nonotify 关闭错误信息报警
    -noexit -noexit 不自动退出
    -autorest -autorest 退出后自动重新启动游戏
    -multiclient -multiclient 1个cdkey可以启动多个客户端游戏
    -nohook -nohook 禁止Windows钩子
    -nochar -nochar 禁止角色图像
    -clientexit -clientexit 退出游戏时自动关闭客户端游戏程序
    -noscript -noscript 不读取脚本
    -noplugin -noplugin 不导入Plug-in
    -locale -locale 设置语言为：ENG(英语)，CHI(中文)
    -hookwnd -hookwnd 设置钩子窗口类为
    -hookexe -hookexe 设置钩子版本校验game.exe为
    -servername -servername 设置游戏服务器端名字为
    -title -title 设置窗口标题为
    *********************************************************************
    一般情况下:
    -direct -txt
    -mpq file.mpq
    -w
    -locale chi
    这几个就够用了
    第一个是给另类模式调试用的，修改BIN文件比较麻烦，修改TXT文件，然后生成
    BIN文件，如果不需要生成BIN文件，参数还可以增加一个 -rtx
    第二个也可以用于快速加载CDK文件，把2个CDK的MPQ文件作为2个快捷方式，倒
    装备相当方便。
    第三个应该都知道，窗口模式，不多说。
    第四个是把其他语言转化为繁体中文..... 奥美版推荐使用.......需要鸟语版
    的参数改为 -locale eng 即可

    D2loader常用参数使用说明
    为"Diablo II.exe"创建一个快捷方式，就可以加上相应的参数实现不同的启动方式了，多个参数中用空格分开，如果字符串参数中有空格就用双引号引起来，如:
    -title "Diablo II"，就可以使Diablo窗口标题为"Diablo II"，如果不加双引号就成了"Diablo"

    D2loader会自动加载Diablo文件夹下Plugin文件夹里的文件，而用-pdir参数可是指定别的文件夹
    选出常用的参数说明
    红色为最常用参数
    代表数字, 代表字符串
    -w 以窗口模式运行Diablo
    -lq 低图像质量(挂bot常用)
    -ns 无声模式
    -locale 设置语言为 : ENG(英语)，CHI(中文)
    -title 设置窗口标题为

    -skiptobnet 直接进入Battle.net，只是省了点一下鼠标
    -res800 窗口大小为800x600(仅适用于D2，对D2X无效)
    -res640 窗口大小为640x480(仅适用于D2，对D2X无效)
    -notitle 无窗口标题栏
    -nonotify 关闭错误信息报警而直接退出
    -noexit 不自动退出
    -nochar 禁止角色图像
    -pdir 指定插件所在目录
    -mpq file.mpq 加载文件file.mpq，一般用在加载cdkey.mpq，或者加载Mod
    -txt 加载Mod时常用，一般游戏不用

    比较常用的
    -direct -w -lq -title "n" -pdir d2cn -locale eng E文
    -nonotify -title -lq -w -pdir d2cn 中文
    */
}

void CD2ModCenterDlg::parse_ini(CString filename)
{
    ST_CLIENT_DATA *pstClientData = &m_stModParam.stClientData;
    FILE *fpListFile = NULL;
    char *pStr = NULL;
    char acBuff[D2LOADER_MAXPATH] = {0};
    int argc = 0;
    char *argv[100] = {NULL};

    CStringA strReadData = ReadD2LoaderIni(filename);
    const char *pcTemp = strReadData;
    strncpy_s(acBuff, pcTemp, sizeof(acBuff));

    argv[argc++] = (char *)"D2Loader.exe";

    pStr = acBuff;
    while ( 0 != *pStr )
    {
        if ( ' ' == *pStr || '\t' == *pStr )
        {
            pStr++;
            continue;
        }

        if ( '"' == *pStr )
        {
            pStr++;
            argv[argc++] = pStr;

            while ( '"' != *pStr && 0 != *pStr )
            {
                pStr++;
            }
            *pStr = 0;
            pStr++;
        }
        else
        {
            argv[argc++] = pStr;

            while ( ' ' != *pStr && '\t' != *pStr && 0 != *pStr )
            {
                pStr++;
            }
            *pStr = 0;
            pStr++;
        }
    }

    parse_command(argc, argv, pstClientData);
}

CStringA CD2ModCenterDlg::ParseMod(CString rootDir)
{
    CString strDir = rootDir;
    strDir += "\\*.*";

    // 遍历得到所有文件名
    CFileFind finder;

    m_strVersion = "";

    BOOL bWorking = finder.FindFile(strDir);

    while (bWorking)
    {
        bWorking = finder.FindNextFile();
        if ( !finder.IsDirectory() && !finder.GetFileName().CompareNoCase(_T("D2Loader.ini")) )
        {
            parse_ini(rootDir + "\\" + finder.GetFileName());

            if (PathFileExists(rootDir + _T("\\D2Loader_Extra.ini")))
            {
                CStringA strReadData = ReadD2LoaderIni(rootDir + _T("\\D2Loader_Extra.ini"));
                if (!strReadData.IsEmpty())
                {
                    m_strExtraParam = strReadData;
                    m_strExtraParam.Replace(_T("^^&&##"), _T("\r\n"));
                }
                else
                {
                    m_strExtraParam = _T("");
                }
            }
            else
            {
                m_strExtraParam = _T("");
            }

            break;
        }
    }
    finder.Close();

    if ("" == m_strVersion)
    {
        CString strTemp = rootDir;
        if ( 0 > strTemp.MakeUpper().Find(_T("\\CORES\\")) )
        {
            return "Unknown";
        }
        else
        {
            m_strVersion = rootDir.Right(5);
        }
    }

    return m_strVersion;
}

void CD2ModCenterDlg::FindModInDir(CString rootDir, CString type, DWORD dwBkColor)
{
    // 查找当前路径下的所有文件夹和文件
    CString strDir = rootDir;
    strDir += "\\*.*";

    // 遍历得到所有子文件夹名
    CFileFind finder;
    BOOL bWorking = finder.FindFile(strDir);

    while (bWorking)
    {
        bWorking = finder.FindNextFile();
        if (finder.IsDirectory() && "." != finder.GetFileName() && ".." != finder.GetFileName())//注意该句需要排除“.”“..”
        {
            m_ModList.InsertItem(m_dwModCount, finder.GetFileName());
            if (!rootDir.Right(5).CompareNoCase(_T("CORES")))
            {
                m_ModList.SetItemText(m_dwModCount, 1, finder.GetFileName());
                m_cbVersion.AddString(finder.GetFileName());
            }
            else
            {
                m_strVersion = "";
                m_ModList.SetItemText(m_dwModCount, 1, CString(ParseMod(rootDir + "\\" + finder.GetFileName())));
            }
            m_ModList.SetItemText(m_dwModCount, 2, rootDir + "\\" + finder.GetFileName());
            m_ModList.SetItemText(m_dwModCount, 3, type);
            m_ModList.SetItemData(m_dwModCount, dwBkColor);

            m_dwModCount++;
        }
    }
    finder.Close();
}

void CD2ModCenterDlg::FindLocaleInDir(CString rootDir)
{
    // 查找当前路径下的所有文件夹和文件
    CString strDir = rootDir;
    strDir += "\\*.*";

    // 遍历得到所有子文件夹名
    CFileFind finder;
    BOOL bWorking = finder.FindFile(strDir);

    m_cbLocalePath.ResetContent();
    m_strTempLanguage.LoadString(IDS_LOCALEAUTO);
    m_cbLocalePath.AddString(m_strTempLanguage);

    while (bWorking)
    {
        bWorking = finder.FindNextFile();
        if (finder.IsDirectory() && "." != finder.GetFileName() && ".." != finder.GetFileName())//注意该句需要排除“.”“..”
        {
            if (!finder.GetFileName().Left(9).CompareNoCase(_T("Language_")))
            {
                m_cbLocalePath.AddString(finder.GetFileName().Right(3));
            }
        }
    }
    finder.Close();
}

BOOL CD2ModCenterDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    writeRegIntValue(HKEY_CURRENT_USER, "Software\\Blizzard Entertainment\\Diablo II", "AllowHardcore", 4, 1);

    m_strDefaultTip.LoadString(IDS_PROMPTINFO);

    m_astrPrompt[0] = m_strDefaultTip;
    m_strTempLanguage.LoadString(IDS_STRING153);
    m_astrPrompt[1] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING154);
    m_astrPrompt[2] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING147);
    m_astrPrompt[3] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING149);
    m_astrPrompt[4] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING150);
    m_astrPrompt[5] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING157);
    m_astrPrompt[6] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING158);
    m_astrPrompt[7] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING159);
    m_astrPrompt[8] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING160);
    m_astrPrompt[9] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING161);
    m_astrPrompt[10] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING162);
    m_astrPrompt[11] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING163);
    m_astrPrompt[12] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING164);
    m_astrPrompt[13] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING165);
    m_astrPrompt[14] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING166);
    m_astrPrompt[15] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING167);
    m_astrPrompt[16] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING168);
    m_astrPrompt[17] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING170);
    m_astrPrompt[18] = m_strTempLanguage;
    m_strTempLanguage.LoadString(IDS_STRING171);
    m_astrPrompt[19] = m_strTempLanguage;

    SetTimer(3, 8000, NULL);

    // 将“关于...”菜单项添加到系统菜单中。

    // IDM_ABOUTBOX 必须在系统命令范围内。
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu* pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != nullptr)
    {
        BOOL bNameValid;
        CString strAboutMenu;
        bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
        ASSERT(bNameValid);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

    // 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
    //  执行此操作
    SetIcon(m_hIcon, TRUE);         // 设置大图标
    SetIcon(m_hIcon, FALSE);        // 设置小图标

    SetWindowText(m_strMyVersion);

    // TODO: 在此添加额外的初始化代码
    GetCurrentDirectory(D2LOADER_MAXPATH, m_acWorkingPath);
    while ( '\\' == m_acWorkingPath[wcslen(m_acWorkingPath) - 1] )
    {
        m_acWorkingPath[wcslen(m_acWorkingPath) - 1] = 0;
    }
    m_strWorkingPath = m_acWorkingPath;

    CRect rect;

    // 获取编程语言列表视图控件的位置和大小
    m_ModList.GetClientRect(&rect);

    // 为列表视图控件添加全行选中和栅格风格
    m_ModList.SetExtendedStyle(m_ModList.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    // 为列表视图控件添加两列
    m_strTempLanguage.LoadString(IDS_STRING105);
    m_ModList.InsertColumn(0, m_strTempLanguage, LVCFMT_CENTER, rect.Width() * 3 / 4, 0);
    m_strTempLanguage.LoadString(IDS_STRING106);
    m_ModList.InsertColumn(1, m_strTempLanguage, LVCFMT_CENTER, rect.Width() / 4, 1);
    m_strTempLanguage.LoadString(IDS_STRING107);
    m_ModList.InsertColumn(2, m_strTempLanguage, LVCFMT_CENTER, 0, 2);
    m_strTempLanguage.LoadString(IDS_STRING108);
    m_ModList.InsertColumn(3, m_strTempLanguage, LVCFMT_CENTER, 0, 2);

    CFont font;
    font.CreatePointFont(100, _T("宋体"));
    m_ModList.SetFont(&font, true);

    m_cbVersion.ResetContent();
    FindModInDir(m_strWorkingPath + CString("\\CORES"), CString("core"), COLOR_GREEN);
    FindModInDir(m_strWorkingPath + CString("\\MODS"), CString("user"), COLOR_DEFAULT);

    m_cbVideoMode.AddString(_T("DDraw"));
    m_cbVideoMode.AddString(_T("Glide"));
    m_cbVideoMode.AddString(_T("CNC-DDraw"));
    m_cbVideoMode.AddString(_T("D2DX"));
    m_cbVideoMode.AddString(_T("D2DX510"));
    m_cbVideoMode.AddString(_T("D3D"));

    //按钮等鼠标悬浮提示初始化
    m_ToolTip.Create(this);
    m_strTempLanguage.LoadString(IDS_STRING109);
    m_ToolTip.AddTool(GetDlgItem(IDC_LISTMOD), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING110);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNSTART), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING111);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNHELP), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING112);
    m_ToolTip.AddTool(GetDlgItem(IDC_COMBO_VER), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING113);
    m_ToolTip.AddTool(GetDlgItem(IDC_COMBO_VIDEO), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING114);
    m_ToolTip.AddTool(GetDlgItem(IDC_COMBO_LOCALE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING115);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNHACKPRE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING115);
    m_ToolTip.AddTool(GetDlgItem(IDC_HACKPRE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING116);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNHACK), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING116);
    m_ToolTip.AddTool(GetDlgItem(IDC_HACK), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING117);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKPLUGY), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING118);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKWINDOW), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING119);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKMULTIOPEN), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING120);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKXP), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING121);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKDEPFIX), m_strTempLanguage);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKDIRECT), _T("direct"));
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKTXT), _T("txt"));
    m_strTempLanguage.LoadString(IDS_STRING122);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKNOSOUND), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING123);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKNOTITLE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING124);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKNOBORDER), m_strTempLanguage);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKSKIPTOBNET), _T("skiptobnet"));
    m_strTempLanguage.LoadString(IDS_STRING125);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKNOHIDE), m_strTempLanguage);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKNOSLEEP), _T("nosleep"));
    m_strTempLanguage.LoadString(IDS_STRING126);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKCONSOLE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING127);
    m_ToolTip.AddTool(GetDlgItem(IDC_TITLE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING128);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKMPQPATH), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING151);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNMPQPATH), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING129);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNDELMPQPATH), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING130);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKMPQFILE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING131);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNMPQFILE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING132);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNDELMPQFILE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING133);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKDLLPATH), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING134);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNDLLPATH), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING135);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNDELDLLPATH), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING136);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKDLLFILE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING137);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNDLLFILE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING138);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNDELDLLFILE), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING139);
    m_ToolTip.AddTool(GetDlgItem(IDC_RUNCMD), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING142);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTND2VIDTST), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING144);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKCOPYD2HD), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING145);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNKILLD2), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING146);
    m_ToolTip.AddTool(GetDlgItem(IDC_BTNBASEMOD), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING169);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKRESTOREDLL), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING172);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKCOMMONDLL), m_strTempLanguage);
    m_strTempLanguage.LoadString(IDS_STRING173);
    m_ToolTip.AddTool(GetDlgItem(IDC_CHECKALLDLL), m_strTempLanguage);
    m_ToolTip.SetMaxTipWidth(1024);
    m_ToolTip.SetDelayTime(0); //设置延迟，如果为0则不等待，立即显示
    m_ToolTip.SetTipTextColor(RGB(0, 0, 255)); //设置提示文本的颜色
    m_ToolTip.SetTipBkColor(RGB(255, 255, 255)); //设置提示框的背景颜色
    m_ToolTip.SetFont(&font, true);
    //m_ToolTip.SetMaxTipWidth(600);//设置文本框的最大宽度，注意里边的数值单位为像素，所以要通过不断测试来选定最理想的宽度。利用此句可显示多行
    m_ToolTip.Activate(TRUE); //设置是否启用提示
    //按钮等鼠标悬浮提示初始化完毕

    SetDlgItemText(IDC_PROMPT, m_strDefaultTip);

    TCHAR ReturnedString[D2LOADER_MAXPATH] = {0};
    GetPrivateProfileString(_T("USERSETTINGS"), _T("ActiveFolder"), _T(""), ReturnedString, D2LOADER_MAXPATH, m_strWorkingPath + _T("\\D2ModCenter.ini"));
    for ( DWORD i = 0; i < m_dwModCount && 0 != ReturnedString[0]; i++ )
    {
        CString strModPath = m_ModList.GetItemText(i, 2);
        if ( !strModPath.CompareNoCase(ReturnedString) )
        {
            m_ModList.SetItemState(i, LVNI_FOCUSED | LVIS_SELECTED, LVNI_FOCUSED | LVIS_SELECTED);
            SelectOneMod(i);
            break;
        }
    }

    return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CD2ModCenterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        DWORD dwOldLocale = m_dwCurrentLocale;
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();

        if ( dwOldLocale != m_dwCurrentLocale )
        {
            m_strTempLanguage.LoadString(IDS_CHANGELOCALE);
            CString strTemp2;
            strTemp2.LoadString(IDS_STRING152);
            if (::MessageBox(NULL, m_strTempLanguage, strTemp2, MB_YESNO) == IDYES)
            {
                // 写入配置文件
                CStdioFile file;
                CString strTemp;
                strTemp.Format(_T("%d"), m_dwCurrentLocale);
                file.Open(_T("Language.ini"), CFile::modeWrite | CFile::modeCreate | CFile::typeText);
                file.WriteString(strTemp);
                file.Close();

                // 关闭窗口
                m_bRestartFlag = TRUE;
                PostMessage(WM_CLOSE, 0, 0);
            }
            else
            {
                m_dwCurrentLocale = dwOldLocale;
            }
        }

        if (TRUE == m_boolTest)
        {
            m_boolWaitGameFinish = TRUE;
            SetTimer(1, 3000, NULL);
            m_strTempLanguage.LoadString(IDS_STRING153);
            SetDlgItemText(IDC_PROMPT, m_strTempLanguage);
        }
        else
        {
            m_boolWaitGameFinish = FALSE;
            KillTimer(1);
            KillTimer(2);
            SetDlgItemText(IDC_PROMPT, m_strDefaultTip);
        }
    }
    else
    {
        CDialogEx::OnSysCommand(nID, lParam);
    }
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CD2ModCenterDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // 用于绘制的设备上下文

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // 使图标在工作区矩形中居中
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // 绘制图标
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CD2ModCenterDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

void CD2ModCenterDlg::UpdateCmdParam()
{
    CString strTemp;

    SetCurrentDirectory(m_strWorkingPath);  //打开文件对话框会改变当前路径，所以每次都要记得恢复一下

    m_strCmdParam = _T("");

    if (m_strModPath.IsEmpty())
    {
        return;
    }

    if ( LB_ERR == m_cbVersion.GetCurSel() )
    {
        return;
    }

    m_cbVersion.GetLBText(m_cbVersion.GetCurSel(), strTemp);
    m_strCmdParam += _T("-ver ") + strTemp + _T("\r\n");

    if ( 1 == ((CButton *)GetDlgItem(IDC_CHECKWINDOW))->GetCheck() )
    {
        m_strCmdParam += _T("-w\r\n");
    }

    m_strCmdParam += _T("-modpath \"") + m_strModPath + _T("\"\r\n");

    m_cbVideoMode.GetLBText(m_cbVideoMode.GetCurSel(), strTemp);
    if (!strTemp.CompareNoCase(_T("Glide")))
    {
        m_strCmdParam += _T("-3dfx\r\n");
        if (1 == ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->GetCheck())
        {
            m_strCmdParam += _T("-d2dx \"") + m_strWorkingPath + _T("\\Tools\\D2HD\\glide3x.dll\"\r\n");
        }
    }
    else if (!strTemp.CompareNoCase(_T("D3D")))
    {
        m_strCmdParam += _T("-d3d\r\n");
        if (1 == ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->GetCheck())
        {
            m_strCmdParam += _T("-d2dx \"") + m_strWorkingPath + _T("\\Tools\\D2HD\\glide3x.dll\"\r\n");
        }
    }
    else if (!strTemp.CompareNoCase(_T("CNC-DDraw")))
    {
        m_strCmdParam += _T("-ddraw \"") + m_strWorkingPath + _T("\\Tools\\cnc-ddraw\\ddraw.dll\"\r\n");
        if (1 == ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->GetCheck())
        {
            m_strCmdParam += _T("-d2dx \"") + m_strWorkingPath + _T("\\Tools\\D2HD\\glide3x.dll\"\r\n");
        }
    }
    else if (!strTemp.CompareNoCase(_T("D2DX")) )
    {
        m_strCmdParam += _T("-3dfx\r\n");
        if (1 == ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->GetCheck())
        {
            m_strCmdParam += _T("-d2dx \"") + m_strWorkingPath + _T("\\Tools\\d2dx510\\glide3x.dll\"\r\n");
        }
        else
        {
            m_strCmdParam += _T("-d2dx \"") + m_strWorkingPath + _T("\\Tools\\d2dx\\glide3x.dll\"\r\n");
        }
    }
    else if (!strTemp.CompareNoCase(_T("D2DX510")))
    {
        m_strCmdParam += _T("-3dfx\r\n");
        m_strCmdParam += _T("-d2dx \"") + m_strWorkingPath + _T("\\Tools\\d2dx510\\glide3x.dll\"\r\n");
    }
    else if (1 == ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->GetCheck())
    {
        m_strCmdParam += _T("-d2dx \"") + m_strWorkingPath + _T("\\Tools\\D2HD\\glide3x.dll\"\r\n");
    }

    m_cbLocalePath.GetLBText(m_cbLocalePath.GetCurSel(), strTemp);
    m_strTempLanguage.LoadString(IDS_LOCALEAUTO);
    if (strTemp.CompareNoCase(m_strTempLanguage))
    {
        m_strCmdParam += _T("-locale ") + strTemp + _T("\r\n");
    }

    if ( PathFileExists(m_strWorkingPath + _T("\\CORES\\") + CString(m_strVersion) + _T("\\d2hackspre.script")) )
    {
        m_strCmdParam += _T("-hackpre0 \"") + m_strWorkingPath + _T("\\CORES\\") + CString(m_strVersion) + _T("\\d2hackspre.script\"") + _T("\r\n");
    }

    GetDlgItemText(IDC_HACKPRE, strTemp);
    if (!strTemp.IsEmpty())
    {
        m_strCmdParam += _T("-hackpre \"") + strTemp + _T("\"\r\n");
    }

    if ( PathFileExists(m_strWorkingPath + _T("\\CORES\\") + CString(m_strVersion) + _T("\\d2hacks.script")) )
    {
        m_strCmdParam += _T("-hack0 \"") + m_strWorkingPath + _T("\\CORES\\") + CString(m_strVersion) + _T("\\d2hacks.script\"") + _T("\r\n");
    }

    GetDlgItemText(IDC_HACK, strTemp);
    if (!strTemp.IsEmpty())
    {
        m_strCmdParam += _T("-hack \"") + strTemp + _T("\"\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKMULTIOPEN))->GetCheck())
    {
        m_strCmdParam += _T("-multiopen\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKXP))->GetCheck())
    {
        m_strCmdParam += _T("-xp\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKDEPFIX))->GetCheck())
    {
        m_strCmdParam += _T("-depfix\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKDIRECT))->GetCheck())
    {
        m_strCmdParam += _T("-direct\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKTXT))->GetCheck())
    {
        m_strCmdParam += _T("-txt\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKNOSOUND))->GetCheck())
    {
        m_strCmdParam += _T("-ns\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKNOTITLE))->GetCheck())
    {
        m_strCmdParam += _T("-notitle\r\n");
        ((CEdit*)GetDlgItem(IDC_TITLE))->SetReadOnly(1);
    }
    else
    {
        ((CEdit*)GetDlgItem(IDC_TITLE))->SetReadOnly(0);
        GetDlgItemText(IDC_TITLE, strTemp);
        if (!strTemp.IsEmpty())
        {
            m_strCmdParam += _T("-title \"") + strTemp + _T("\"\r\n");
        }
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKNOBORDER))->GetCheck())
    {
        m_strCmdParam += _T("-noborder\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKSKIPTOBNET))->GetCheck())
    {
        m_strCmdParam += _T("-skiptobnet\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKNOHIDE))->GetCheck())
    {
        m_strCmdParam += _T("-nohide\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKNOSLEEP))->GetCheck())
    {
        m_strCmdParam += _T("-nosleep\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKCONSOLE))->GetCheck())
    {
        m_strCmdParam += _T("-console\r\n");
    }

    /*
    MPQ搜索顺序：
    1、mod path(D2Loader内置处理)
    2、aacGlobalMpqPath
    3、core
    4、working path
    */
    if (1 == ((CButton*)GetDlgItem(IDC_CHECKMPQPATH))->GetCheck())
    {
        ((CButton*)GetDlgItem(IDC_BTNMPQPATH))->EnableWindow(TRUE);
        ((CButton*)GetDlgItem(IDC_BTNDELMPQPATH))->EnableWindow(TRUE);

        for (DWORD i = 0; i < m_stModParam.dwGlobalMpqPath; ++i)
        {
            strTemp = m_stModParam.aacGlobalMpqPath[i];
            if ( !strTemp.CompareNoCase(m_strWorkingPath) 
                || !strTemp.CompareNoCase(m_strWorkingPath + _T("\\"))
                || !(strTemp + _T("\\")).CompareNoCase(m_strWorkingPath) )
            {
                //避免重复添加当前工作路径
                continue;
            }
            if ( 0 <= strTemp.MakeUpper().Find(_T("\\CORES\\")) )
            {
                //避免重复添加CORE
                continue;
            }
            m_strCmdParam += _T("-mpqpath \"") + CString(m_stModParam.aacGlobalMpqPath[i]) + _T("\"\r\n");
        }
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_BTNMPQPATH))->EnableWindow(FALSE);
        ((CButton*)GetDlgItem(IDC_BTNDELMPQPATH))->EnableWindow(FALSE);
    }

    m_strCmdParam += _T("-mpqpath \"") + m_strWorkingPath + _T("\\CORES\\") + CString(m_strVersion) + _T("\"\r\n");
    m_strCmdParam += _T("-mpqpath \"") + m_strWorkingPath + _T("\"\r\n");

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKMPQFILE))->GetCheck())
    {
        ((CButton*)GetDlgItem(IDC_BTNMPQFILE))->EnableWindow(TRUE);
        ((CButton*)GetDlgItem(IDC_BTNDELMPQFILE))->EnableWindow(TRUE);

        for (DWORD i = 0; i < m_stModParam.dwExtendMpq; ++i)
        {
            m_strCmdParam += _T("-mpq \"") + CString(m_stModParam.aacExtendMpq[i]) + _T("\"\r\n");
        }
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_BTNMPQFILE))->EnableWindow(FALSE);
        ((CButton*)GetDlgItem(IDC_BTNDELMPQFILE))->EnableWindow(FALSE);
    }

    /*
    DLL搜索顺序：
    1、mod path(D2Loader内置处理)
    2、aacGlobalDllPath
    3、core
    4、working path
    */
    if (1 == ((CButton*)GetDlgItem(IDC_CHECKDLLPATH))->GetCheck())
    {
        ((CButton*)GetDlgItem(IDC_BTNDLLPATH))->EnableWindow(TRUE);
        ((CButton*)GetDlgItem(IDC_BTNDELDLLPATH))->EnableWindow(TRUE);

        for (DWORD i = 0; i < m_stModParam.dwGlobalDllPath; ++i)
        {
            strTemp = m_stModParam.aacGlobalDllPath[i];
            if ( !strTemp.CompareNoCase(m_strWorkingPath) 
                || !strTemp.CompareNoCase(m_strWorkingPath + _T("\\"))
                || !(strTemp + _T("\\")).CompareNoCase(m_strWorkingPath) )
            {
                //避免重复添加当前工作路径
                continue;
            }
            if ( 0 <= strTemp.MakeUpper().Find(_T("\\CORES\\")) )
            {
                //避免重复添加CORE
                continue;
            }

            m_strCmdParam += _T("-dllpath \"") + CString(m_stModParam.aacGlobalDllPath[i]) + _T("\"\r\n");
        }
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_BTNDLLPATH))->EnableWindow(FALSE);
        ((CButton*)GetDlgItem(IDC_BTNDELDLLPATH))->EnableWindow(FALSE);
    }

    m_strCmdParam += _T("-dllpath \"") + m_strWorkingPath + _T("\\CORES\\") + CString(m_strVersion) + _T("\"\r\n");
    m_strCmdParam += _T("-dllpath \"") + m_strWorkingPath + _T("\"\r\n");

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKPLUGY))->GetCheck())
    {
        if ( !m_strModPath.IsEmpty() && PathFileExists(m_strModPath + "\\PlugY.dll") )
        {
            m_strCmdParam += _T("-plugin \"") + m_strModPath + _T("\\PlugY.dll\"\r\n");
        }
        else
        {
            m_strCmdParam += _T("-plugin PlugY.dll\r\n");
        }
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->GetCheck())
    {
        m_strCmdParam += _T("-plugin \"") + m_strWorkingPath + _T("\\Tools\\D2HD\\D2HD.dll\"\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKDLLFILE))->GetCheck())
    {
        ((CButton*)GetDlgItem(IDC_BTNDLLFILE))->EnableWindow(TRUE);
        ((CButton*)GetDlgItem(IDC_BTNDELDLLFILE))->EnableWindow(TRUE);

        for (DWORD i = 0; i < m_stModParam.dwExtendPlugin; ++i)
        {
            strTemp = m_stModParam.aacExtendPlugin[i];
            if ( 0 <= strTemp.MakeUpper().Find(_T("PLUGY.DLL"))  )
            {
                //PlugY.dll单独处理
                continue;
            }
            if ( 0 <= strTemp.MakeUpper().Find(_T("D2HD.DLL"))  )
            {
                //D2HD.dll单独处理
                continue;
            }
            m_strCmdParam += _T("-plugin \"") + CString(m_stModParam.aacExtendPlugin[i]) + _T("\"\r\n");
        }
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_BTNDLLFILE))->EnableWindow(FALSE);
        ((CButton*)GetDlgItem(IDC_BTNDELDLLFILE))->EnableWindow(FALSE);
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKRESTOREDLL))->GetCheck())
    {
        m_strCmdParam += _T("-takeplugin\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKCOMMONDLL))->GetCheck())
    {
        m_strCmdParam += _T("-d2common\r\n");
    }

    if (1 == ((CButton*)GetDlgItem(IDC_CHECKALLDLL))->GetCheck())
    {
        m_strCmdParam += _T("-showalldll\r\n");
    }

    SetDlgItemText(IDC_RUNCMD, m_strCmdParam + m_strExtraParam);
}

void CD2ModCenterDlg::SelectOneMod(int iModIndex)
{
    CString strLangName;    // 选择语言的名称字符串

    resetClientData();

    SetDlgItemText(IDC_CURRENTMOD, _T(""));
    m_strModPath = _T("");
    m_cbLocalePath.ResetContent();
    m_cbVideoMode.SetCurSel(m_cbVideoMode.FindStringExact(0, _T("DDraw")));
    SetDlgItemText(IDC_RUNCMD, _T(""));
    SetDlgItemText(IDC_HACKPRE, _T(""));
    SetDlgItemText(IDC_HACK, _T(""));
    SetDlgItemText(IDC_TITLE, _T(""));

    ((CButton*)GetDlgItem(IDC_CHECKWINDOW))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKMULTIOPEN))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKXP))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKDEPFIX))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKDIRECT))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKTXT))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKNOSOUND))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKNOTITLE))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKNOBORDER))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKSKIPTOBNET))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKNOHIDE))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKNOSLEEP))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKCONSOLE))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKMPQPATH))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKMPQFILE))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKDLLPATH))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKDLLFILE))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKPLUGY))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_BTNSTART))->EnableWindow(FALSE);

    m_strCmdParam = _T("");
    m_strExtraParam = _T("");

    m_iModIndex = iModIndex;

    if (-1 != m_iModIndex)   // 如果iItem不是-1，就说明有列表项被选择
    {
        strLangName = m_ModList.GetItemText(m_iModIndex, 2);
        m_ModList.SetItemText(m_iModIndex, 1, CString(ParseMod(strLangName)));
        updateParamByDefault();

        SetDlgItemText(IDC_CURRENTMOD, strLangName);
        GetDlgItemText(IDC_CURRENTMOD, m_strModPath);
        m_strModType = m_ModList.GetItemText(m_iModIndex, 3);
        m_strVersion = m_ModList.GetItemText(m_iModIndex, 1);
        m_cbVersion.SetCurSel(m_cbVersion.FindStringExact(0, m_ModList.GetItemText(m_iModIndex, 1)));

        FindLocaleInDir(strLangName);
        if (0 != m_stModParam.acLanguageMpq[0])
        {
            strLangName = m_stModParam.acLanguageMpq;
            int start = strLangName.Find(_T("\\"));
            int end = strLangName.Find(_T(".mpq"));
            m_cbLocalePath.SetCurSel(m_cbLocalePath.FindStringExact(0, strLangName.Mid(start+1, end-start-1)));
        }
        else
        {
            m_strTempLanguage.LoadString(IDS_LOCALEAUTO);
            m_cbLocalePath.SetCurSel(m_cbLocalePath.FindStringExact(0, m_strTempLanguage));
        }

        CString strTemp;
        m_cbVideoMode.GetLBText(m_cbVideoMode.GetCurSel(), strTemp);
        if (strTemp.CompareNoCase(_T("CNC-DDraw")))
        {
            ((CButton*)GetDlgItem(IDC_CHECKWINDOW))->SetCheck(1);
        }

        ((CButton*)GetDlgItem(IDC_CHECKXP))->SetCheck(1);
        UpdateCmdParam();

        if ( LB_ERR != m_cbVersion.GetCurSel() )
        {
            ((CButton*)GetDlgItem(IDC_BTNSTART))->EnableWindow(TRUE);
        }
    }
}

void CD2ModCenterDlg::OnNMClickListmod(NMHDR* pNMHDR, LRESULT* pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    // TODO: 在此添加控件通知处理程序代码
    NMLISTVIEW *pNMListView = (NMLISTVIEW*)pNMHDR;

    SelectOneMod(pNMListView->iItem);

    *pResult = 0;
}

static char* CP_stristr(const char *pcString1, const char *pcString2)
{
    char *pCompareStart = (char *)pcString1;
    char *pCursor_S1, *pCursor_S2;
    char cSrc, cDst;

    // If there is a null source string - this is a "no match"

    if (!pcString1)
    {
        return NULL;
    }

    // Null length string 2 - this is a "no match"
    if (!*pcString2)
    {
        return NULL;
    }

    // Search from every start pos in the source string
    while (*pCompareStart)
    {
        pCursor_S1 = pCompareStart;
        pCursor_S2 = (char *)pcString2;

        // Scan both string

        while (*pCursor_S1 && *pCursor_S2)
        {
            cSrc = *pCursor_S1;
            cDst = *pCursor_S2;

            // Correct case

            if ((cSrc >= 'A') && (cSrc <= 'Z'))
            {
                cSrc -= ('A' - 'a');
            }

            if ((cDst >= 'A') && (cDst <= 'Z'))
            {
                cDst -= ('A' - 'a');
            }

            if (cSrc != cDst)
            {
                break;
            }

            pCursor_S1++;
            pCursor_S2++;
        }

        // If string 2 is exhausted - there is a match

        if (!*pCursor_S2)
        {
            return pCompareStart;
        }

        // Offset source and continue
        pCompareStart++;
    }

    return NULL;
}

void CD2ModCenterDlg::updateParamByDefault()
{
    CString strTemp;
    char acTemp[D2LOADER_MAXPATH] = { 0 };

    if (m_stModParam.stClientData.glide_mode)
    {
        if ( (0 == m_stModParam.acD2DxPlugin[0]) || (NULL != CP_stristr(m_stModParam.acD2DxPlugin, "\\D2HD\\glide3x.dll")) )
        {
            m_cbVideoMode.SetCurSel(m_cbVideoMode.FindStringExact(0, _T("Glide")));
        }
        else if ( NULL != CP_stristr(m_stModParam.acD2DxPlugin, "\\d2dx510\\glide3x.dll") )
        {
            m_cbVideoMode.SetCurSel(m_cbVideoMode.FindStringExact(0, _T("D2DX510")));
        }
        else
        {
            m_cbVideoMode.SetCurSel(m_cbVideoMode.FindStringExact(0, _T("D2DX")));
        }
    }
    else if (m_stModParam.stClientData.d3d_mode)
    {
        m_cbVideoMode.SetCurSel(m_cbVideoMode.FindStringExact(0, _T("D3D")));
    }
    else
    {
        if ( 0 != m_stModParam.acDDrawPlugin[0] && (NULL != CP_stristr(m_stModParam.acDDrawPlugin, "\\cnc-ddraw\\ddraw.dll")) )
        {
            m_cbVideoMode.SetCurSel(m_cbVideoMode.FindStringExact(0, _T("CNC-DDRAW")));
        }
        else
        {
            m_cbVideoMode.SetCurSel(m_cbVideoMode.FindStringExact(0, _T("DDRAW")));
        }
    }

    if (NULL != CP_stristr(m_stModParam.acD2DxPlugin, "D2HD.dll"))
    {
        ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->SetCheck(0);
    }

    if (m_stModParam.stClientData.window_mode)
    {
        ((CButton*)GetDlgItem(IDC_CHECKWINDOW))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKWINDOW))->SetCheck(0);
    }

    if (m_stModParam.stClientData.direct)
    {
        ((CButton*)GetDlgItem(IDC_CHECKDIRECT))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKDIRECT))->SetCheck(0);
    }

    if (m_stModParam.stClientData.txt)
    {
        ((CButton*)GetDlgItem(IDC_CHECKTXT))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKTXT))->SetCheck(0);
    }

    if (m_stModParam.stClientData.no_sound)
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOSOUND))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOSOUND))->SetCheck(0);
    }

    if (m_stModParam.stClientData.skip_to_bnet)
    {
        ((CButton*)GetDlgItem(IDC_CHECKSKIPTOBNET))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKSKIPTOBNET))->SetCheck(0);
    }

    if (m_stModParam.boolMultiOpen)
    {
        ((CButton*)GetDlgItem(IDC_CHECKMULTIOPEN))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKMULTIOPEN))->SetCheck(0);
    }

    if (m_stModParam.boolXpCompatible)
    {
        ((CButton*)GetDlgItem(IDC_CHECKXP))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKXP))->SetCheck(0);
    }

    if (m_stModParam.boolDepFix)
    {
        ((CButton*)GetDlgItem(IDC_CHECKDEPFIX))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKDEPFIX))->SetCheck(0);
    }

    if (m_stModParam.boolNoBorder)
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOBORDER))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOBORDER))->SetCheck(0);
    }

    if ( !_stricmp(m_stModParam.acGameTitle, "notitle") )
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOTITLE))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOTITLE))->SetCheck(0);
        SetDlgItemText(IDC_TITLE, CString(m_stModParam.acGameTitle));
    }

    if ( 0 != m_stModParam.acHackScriptPre[0] )
    {
        SetDlgItemText(IDC_HACKPRE, CString(m_stModParam.acHackScriptPre));
    }
    else
    {
        SetDlgItemText(IDC_HACKPRE, CString(""));
    }

    if (0 != m_stModParam.acHackScript[0])
    {
        SetDlgItemText(IDC_HACK, CString(m_stModParam.acHackScript));
    }
    else
    {
        SetDlgItemText(IDC_HACK, CString(""));
    }

    if (m_stModParam.boolNoHide)
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOHIDE))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOHIDE))->SetCheck(0);
    }

    if (m_stModParam.boolNoSleep)
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOSLEEP))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKNOSLEEP))->SetCheck(0);
    }

    if (m_stModParam.boolWithConsole)
    {
        ((CButton*)GetDlgItem(IDC_CHECKCONSOLE))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKCONSOLE))->SetCheck(0);
    }

    if (2 < m_stModParam.dwGlobalMpqPath)
    {
        //除了working path和core path还有指定mpq path
        ((CButton*)GetDlgItem(IDC_CHECKMPQPATH))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKMPQPATH))->SetCheck(0);
    }

    if (2 < m_stModParam.dwGlobalDllPath)
    {
        //除了working path和core path还有指定dll path
        ((CButton*)GetDlgItem(IDC_CHECKDLLPATH))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKDLLPATH))->SetCheck(0);
    }

    if (0 < m_stModParam.dwExtendMpq)
    {
        ((CButton*)GetDlgItem(IDC_CHECKMPQFILE))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKMPQFILE))->SetCheck(0);
    }

    ((CButton*)GetDlgItem(IDC_CHECKPLUGY))->SetCheck(0);
    ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->SetCheck(0);

    for (size_t i = 0; i < m_stModParam.dwExtendPlugin; i++)
    {
        strTemp = m_stModParam.aacExtendPlugin[i];
        if ( 0 <= strTemp.MakeUpper().Find(_T("PLUGY.DLL")) )
        {
            ((CButton*)GetDlgItem(IDC_CHECKPLUGY))->SetCheck(1);
        }
        else if ( 0 <= strTemp.MakeUpper().Find(_T("D2HD.DLL")) )
        {
            ((CButton*)GetDlgItem(IDC_CHECKCOPYD2HD))->SetCheck(1);
        }
        else if (0 == ((CButton*)GetDlgItem(IDC_CHECKDLLFILE))->GetCheck())
        {
            ((CButton*)GetDlgItem(IDC_CHECKDLLFILE))->SetCheck(1);
        }
    }

    if (0 < m_stModParam.boolTakePlugin)
    {
        ((CButton*)GetDlgItem(IDC_CHECKRESTOREDLL))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKRESTOREDLL))->SetCheck(0);
    }

    if (0 < m_stModParam.boolD2Common)
    {
        ((CButton*)GetDlgItem(IDC_CHECKCOMMONDLL))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKCOMMONDLL))->SetCheck(0);
    }

    if (0 < m_stModParam.boolShowAllDll)
    {
        ((CButton*)GetDlgItem(IDC_CHECKALLDLL))->SetCheck(1);
    }
    else
    {
        ((CButton*)GetDlgItem(IDC_CHECKALLDLL))->SetCheck(0);
    }
}

void CD2ModCenterDlg::OnNMCustomdrawListmod(NMHDR* pNMHDR, LRESULT* pResult)
{
    LPNMTVCUSTOMDRAW pNMCD = reinterpret_cast<LPNMTVCUSTOMDRAW>(pNMHDR);
    // TODO: 在此添加控件通知处理程序代码
    NMCUSTOMDRAW nmCustomDraw = pNMCD->nmcd;
    switch (nmCustomDraw.dwDrawStage)
    {
    case CDDS_ITEMPREPAINT:
    {
        if (COLOR_BLUE == nmCustomDraw.lItemlParam)
        {
            pNMCD->clrTextBk = RGB(51, 153, 255);
            pNMCD->clrText = RGB(255, 255, 255);
        }
        else if (COLOR_GREEN == nmCustomDraw.lItemlParam)
        {
            pNMCD->clrTextBk = RGB(11, 140, 30);      //背景颜色
            pNMCD->clrText = RGB(255, 255, 255);        //文字颜色
        }
        else if (COLOR_DEFAULT == nmCustomDraw.lItemlParam)
        {
            pNMCD->clrTextBk = RGB(255, 255, 255);
            pNMCD->clrText = RGB(0, 0, 0);
        }
        else
        {
            //
        }
        break;
    }
    default:
    {
        break;
    }
    }

    *pResult = 0;
    *pResult |= CDRF_NOTIFYPOSTPAINT;       //必须有，不然就没有效果
    *pResult |= CDRF_NOTIFYITEMDRAW;        //必须有，不然就没有效果
    return;
}

BOOL CD2ModCenterDlg::PreTranslateMessage(MSG* pMsg)
{
    // TODO: 在此添加专用代码和/或调用基类
    if((WM_KEYDOWN == pMsg->message) && (VK_ESCAPE == pMsg->wParam))
    {
        return TRUE;
    }

    m_ToolTip.RelayEvent(pMsg);
    return CDialogEx::PreTranslateMessage(pMsg);
}

void CD2ModCenterDlg::OnCbnSelchangeComboVer()
{
    // TODO: 在此添加控件通知处理程序代码
    if ( LB_ERR == m_cbVersion.GetCurSel() )
    {
        return;
    }

    CString strTemp;
    m_cbVersion.GetLBText(m_cbVersion.GetCurSel(), strTemp);
    m_ModList.SetItemText(m_iModIndex, 1, strTemp);
    m_strVersion = strTemp;

    ((CButton*)GetDlgItem(IDC_BTNSTART))->EnableWindow(TRUE);
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnCbnSelchangeComboVideo()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnCbnSelchangeComboLocale()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckwindow()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnEnChangeHackpre()
{
    // TODO:  如果该控件是 RICHEDIT 控件，它将不
    // 发送此通知，除非重写 CDialogEx::OnInitDialog()
    // 函数并调用 CRichEditCtrl().SetEventMask()，
    // 同时将 ENM_CHANGE 标志“或”运算到掩码中。

    // TODO:  在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnEnChangeHack()
{
    // TODO:  如果该控件是 RICHEDIT 控件，它将不
    // 发送此通知，除非重写 CDialogEx::OnInitDialog()
    // 函数并调用 CRichEditCtrl().SetEventMask()，
    // 同时将 ENM_CHANGE 标志“或”运算到掩码中。

    // TODO:  在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnEnChangeTitle()
{
    // TODO:  如果该控件是 RICHEDIT 控件，它将不
    // 发送此通知，除非重写 CDialogEx::OnInitDialog()
    // 函数并调用 CRichEditCtrl().SetEventMask()，
    // 同时将 ENM_CHANGE 标志“或”运算到掩码中。

    // TODO:  在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckmultiopen()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckxp()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckdepfix()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckdirect()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedChecktxt()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}
void CD2ModCenterDlg::OnBnClickedChecknosound()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedChecknotitle()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedChecknoborder()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckskiptobnet()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedChecknohide()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedChecknosleep()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckconsole()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckmpqpath()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckmpqfile()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckdllpath()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}
void CD2ModCenterDlg::OnBnClickedCheckdllfile()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckplugy()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtndelmpqpath()
{
    // TODO: 在此添加控件通知处理程序代码
    m_stModParam.dwGlobalMpqPath = 0;
    memset(m_stModParam.aacGlobalMpqPath, 0, sizeof(m_stModParam.aacGlobalMpqPath));
    ((CButton*)GetDlgItem(IDC_CHECKMPQPATH))->SetCheck(0);
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtndeldllpath()
{
    // TODO: 在此添加控件通知处理程序代码
    m_stModParam.dwGlobalDllPath = 0;
    memset(m_stModParam.aacGlobalDllPath, 0, sizeof(m_stModParam.aacGlobalDllPath));
    ((CButton*)GetDlgItem(IDC_CHECKDLLPATH))->SetCheck(0);
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtndelmpqfile()
{
    // TODO: 在此添加控件通知处理程序代码
    m_stModParam.dwExtendMpq = 0;
    memset(m_stModParam.aacExtendMpq, 0, sizeof(m_stModParam.aacExtendMpq));
    ((CButton*)GetDlgItem(IDC_CHECKMPQFILE))->SetCheck(0);
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtndeldllfile()
{
    // TODO: 在此添加控件通知处理程序代码
    m_stModParam.dwExtendPlugin = 0;
    memset(m_stModParam.aacExtendPlugin, 0, sizeof(m_stModParam.aacExtendPlugin));
    ((CButton*)GetDlgItem(IDC_CHECKDLLFILE))->SetCheck(0);
    UpdateCmdParam();
}

static int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
    switch(uMsg)
    {
        case BFFM_INITIALIZED:    //初始化消息
            ::SendMessage(hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)(LPTSTR)(LPCTSTR)m_acWorkingPath);
            break;

        case BFFM_SELCHANGED:    //选择路径变化，
            {
            TCHAR curr[D2LOADER_MAXPATH];   
            ::SHGetPathFromIDList((LPCITEMIDLIST)lParam, curr);   
            ::SendMessage(hwnd, BFFM_SETSTATUSTEXT, 0, (LPARAM)curr);   
            }
            break;

        default:
            break;
    }

    return 0;   
}

void CD2ModCenterDlg::OnBnClickedBtnmpqpath()
{
    // TODO: 在此添加控件通知处理程序代码
    if (m_stModParam.dwGlobalMpqPath >= MAX_EXTEND_MPQ_PATH)
    {
        return;
    }

    BROWSEINFO sInfo;
    ::ZeroMemory(&sInfo, sizeof(BROWSEINFO));
    sInfo.pidlRoot = 0;
    m_strTempLanguage.LoadString(IDS_STRING155);
    sInfo.lpszTitle = m_strTempLanguage;
    sInfo.ulFlags = BIF_DONTGOBELOWDOMAIN | BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE | BIF_EDITBOX;
    sInfo.lpfn = (BFFCALLBACK)BrowseCallbackProc;
    CHAR szFolderPath[D2LOADER_MAXPATH] = { 0 };

    // 显示文件夹选择对话框
    LPITEMIDLIST lpidlBrowse = ::SHBrowseForFolder(&sInfo);
    if (lpidlBrowse != NULL)
    {
        // 取得文件夹名
        if (::SHGetPathFromIDListA(lpidlBrowse, szFolderPath))
        {
            strncpy_s(m_stModParam.aacGlobalMpqPath[m_stModParam.dwGlobalMpqPath], szFolderPath, sizeof(m_stModParam.aacGlobalMpqPath[m_stModParam.dwGlobalMpqPath]));
            m_stModParam.dwGlobalMpqPath++;
        }
    }
    if (lpidlBrowse != NULL)
    {
        ::CoTaskMemFree(lpidlBrowse);
    }

    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtndllpath()
{
    // TODO: 在此添加控件通知处理程序代码
    if (m_stModParam.dwGlobalDllPath >= MAX_EXTEND_PLUGIN_PATH)
    {
        return;
    }

    BROWSEINFO sInfo;
    ::ZeroMemory(&sInfo, sizeof(BROWSEINFO));
    sInfo.pidlRoot = 0;
    m_strTempLanguage.LoadString(IDS_STRING155);
    sInfo.lpszTitle = m_strTempLanguage;
    sInfo.ulFlags = BIF_DONTGOBELOWDOMAIN | BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE | BIF_EDITBOX;
    sInfo.lpfn = (BFFCALLBACK)BrowseCallbackProc;
    CHAR szFolderPath[D2LOADER_MAXPATH] = { 0 };

    // 显示文件夹选择对话框
    LPITEMIDLIST lpidlBrowse = ::SHBrowseForFolder(&sInfo);
    if (lpidlBrowse != NULL)
    {
        // 取得文件夹名
        if (::SHGetPathFromIDListA(lpidlBrowse, szFolderPath))
        {
            strncpy_s(m_stModParam.aacGlobalDllPath[m_stModParam.dwGlobalDllPath], szFolderPath, sizeof(m_stModParam.aacGlobalDllPath[m_stModParam.dwGlobalDllPath]));
            m_stModParam.dwGlobalDllPath++;
        }
    }
    if (lpidlBrowse != NULL)
    {
        ::CoTaskMemFree(lpidlBrowse);
    }

    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtnmpqfile()
{
    // TODO: 在此添加控件通知处理程序代码
    if (m_stModParam.dwExtendMpq >= MAX_EXTEND_MPQ)
    {
        return;
    }
    CString gReadFilePathName;
    CFileDialog fileDlg(true, _T("mpq"), _T("*.mpq"), OFN_OVERWRITEPROMPT, _T("mpq Files (*.mpq)"), NULL);
    fileDlg.m_ofn.lpstrInitialDir = m_strModPath;
    if (fileDlg.DoModal() == IDOK)    //弹出对话框
    {
        gReadFilePathName = fileDlg.GetPathName();//得到完整的文件名和目录名拓展名
        //CString filename = fileDlg.GetFileName();
        const wchar_t *pcTemp = gReadFilePathName;
        USES_CONVERSION;
        strncpy_s(m_stModParam.aacExtendMpq[m_stModParam.dwExtendMpq], CW2A(pcTemp), sizeof(m_stModParam.aacExtendMpq[m_stModParam.dwExtendMpq]));
        m_stModParam.dwExtendMpq++;
    }

    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtndllfile()
{
    // TODO: 在此添加控件通知处理程序代码
    if (m_stModParam.dwExtendPlugin >= MAX_EXTEND_PLUGIN)
    {
        return;
    }

    CString gReadFilePathName;
    CFileDialog fileDlg(true, _T("dll"), _T("*.dll"), OFN_OVERWRITEPROMPT, _T("dll Files (*.dll)"), NULL);
    CString strModPath;
    fileDlg.m_ofn.lpstrInitialDir = m_strModPath;
    if (fileDlg.DoModal() == IDOK)    //弹出对话框
    {
        gReadFilePathName = fileDlg.GetPathName();//得到完整的文件名和目录名拓展名
        //CString filename = fileDlg.GetFileName();
        if ( !gReadFilePathName.Right(9).CompareNoCase(_T("PlugY.dll")) )
        {
            //gReadFilePathName += ":_Init@4";
            return; //PlugY统一处理，不允许自己选择
        }
#if 0
        //都改为D2Loader内部处理
        else if ( !gReadFilePathName.Right(9).CompareNoCase(_T("d2mod.dll")) )
        {
            gReadFilePathName += ":#10000";  //10001是d2mod的释放函数
        }
        else if ( !gReadFilePathName.Right(9).CompareNoCase(_T("D2SE_Utility.dll")) )
        {
            gReadFilePathName += ":#10000";  //10001是D2SE_Utility的释放函数
        }
#endif
        const wchar_t *pcTemp = gReadFilePathName;
        USES_CONVERSION;
        strncpy_s(m_stModParam.aacExtendPlugin[m_stModParam.dwExtendPlugin], CW2A(pcTemp), sizeof(m_stModParam.aacExtendPlugin[m_stModParam.dwExtendPlugin]));
        m_stModParam.dwExtendPlugin++;
    }

    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtnhelp()
{
    // TODO: 在此添加控件通知处理程序代码
    PROCESS_INFORMATION pi;
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));

    STARTUPINFO si;
    ZeroMemory(&si, sizeof(STARTUPINFO));     //初始化
    si.cb = sizeof(STARTUPINFO);
    si.wShowWindow = SW_SHOW;
    si.dwFlags = STARTF_USESHOWWINDOW;

    //注意
    wchar_t szCmdline[] = _T("notepad.exe 功能说明.txt");

    if (::CreateProcess(NULL, szCmdline, NULL,
        NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi)) {
    }
}

BOOL CD2ModCenterDlg::UpdateD2LoaderVersion(LPCTSTR lpszFile)
{
    //根据启动的游戏版本号自动调整D2Loader.exe的版本号
    if ( NULL == lpszFile || !PathFileExists(lpszFile) )
    {
        return FALSE;
    }

    BOOL bRet = FALSE;
    DWORD dwHandle = 0;
    DWORD dwSize = 0;

    struct  
    {  
        WORD wLanguage;  
        WORD wCodePage;  
    } *lpTranslate;

    DWORD dwFileVersionMS = 0;
    DWORD dwFileVersionLS = 0;

    if ( !m_strVersion.CompareNoCase("1.09d") )
    {
        dwFileVersionMS = (1 << 16) | 0;
        dwFileVersionLS = (9 << 16) | 22;
    }
    else if ( !m_strVersion.CompareNoCase("1.10f") )
    {
        dwFileVersionMS = (1 << 16) | 0;
        dwFileVersionLS = (10 << 16) | 39;
    }
    else if ( !m_strVersion.CompareNoCase("1.11b") )
    {
        dwFileVersionMS = (1 << 16) | 0;
        dwFileVersionLS = (11 << 16) | 46;
    }
    else if ( !m_strVersion.CompareNoCase("1.12a") )
    {
        dwFileVersionMS = (1 << 16) | 0;
        dwFileVersionLS = (12 << 16) | 49;
    }
    else if ( !m_strVersion.CompareNoCase("1.13d") )
    {
        dwFileVersionMS = (1 << 16) | 0;
        dwFileVersionLS = (13 << 16) | 64;
    }
    else
    {
        //默认1.13C
        dwFileVersionMS = (1 << 16) | 0;
        dwFileVersionLS = (13 << 16) | 60;
    }

    dwSize = GetFileVersionInfoSize(lpszFile, &dwHandle);
    if ( 0 >= dwSize)
    {
        return bRet;
    }

    LPBYTE lpBuffer = new BYTE[dwSize];
    memset( lpBuffer, 0, dwSize );

    if (GetFileVersionInfo(lpszFile, dwHandle, dwSize, lpBuffer) != FALSE)
    {
        HANDLE hResource = BeginUpdateResource(lpszFile, FALSE);
        if (NULL != hResource)
        {
            UINT uTemp;
            DWORD dwVer[4] = {0};

            if (VerQueryValue(lpBuffer, _T("\\VarFileInfo\\Translation"), (LPVOID *)&lpTranslate, &uTemp) != FALSE)
            {
                LPVOID lpFixedBuf = NULL;
                DWORD dwFixedLen = 0;
                if( FALSE != VerQueryValue( lpBuffer, _T("\\"), &lpFixedBuf, (PUINT)&dwFixedLen ))
                {
                    VS_FIXEDFILEINFO* pFixedInfo = (VS_FIXEDFILEINFO*)lpFixedBuf;

                    //pFixedInfo->dwFileVersionLS    = pFixedInfo->dwFileVersionLS + 0x1;
                    //pFixedInfo->dwProductVersionLS = pFixedInfo->dwProductVersionLS + 0x1;
                    if ( pFixedInfo->dwFileVersionMS == dwFileVersionMS && pFixedInfo->dwFileVersionLS == dwFileVersionLS )
                    {
                        goto out;
                    }

                    pFixedInfo->dwFileVersionMS = dwFileVersionMS;
                    pFixedInfo->dwFileVersionLS = dwFileVersionLS;

                    dwVer[0] = HIWORD(pFixedInfo->dwFileVersionMS);
                    dwVer[1] = LOWORD(pFixedInfo->dwFileVersionMS);
                    dwVer[2] = HIWORD(pFixedInfo->dwFileVersionLS);
                    dwVer[3] = LOWORD(pFixedInfo->dwFileVersionLS);
                }

                // 修改版本的文本信息
                LPVOID lpStringBuf = NULL;
                DWORD dwStringLen = 0;
                TCHAR szTemp[D2LOADER_MAXPATH] = {0};
                TCHAR szVersion[D2LOADER_MAXPATH] = {0};

                _stprintf_s( szTemp, D2LOADER_MAXPATH - 1, _T("\\StringFileInfo\\%04x%04x\\FileVersion"), lpTranslate->wLanguage, lpTranslate->wCodePage );
                _stprintf_s( szVersion, D2LOADER_MAXPATH - 1, _T("%d, %d, %d, %d"), dwVer[0], dwVer[1], dwVer[2], dwVer[3] );

                if( FALSE != VerQueryValue( lpBuffer, szTemp, &lpStringBuf, (PUINT)&dwStringLen ) )
                {
                    memcpy( lpStringBuf, szVersion, (_tcslen(szVersion) + 1) * sizeof(TCHAR) );
                }

                memset( szTemp, 0, sizeof(szTemp) );
                _stprintf_s( szTemp, D2LOADER_MAXPATH - 1, _T("\\StringFileInfo\\%04x%04x\\ProductVersion"), lpTranslate->wLanguage, lpTranslate->wCodePage );

                if( FALSE != VerQueryValue( lpBuffer, szTemp, &lpStringBuf, (PUINT)&dwStringLen ) )
                {
                    memcpy( lpStringBuf, szVersion, (_tcslen(szVersion) + 1) * sizeof(TCHAR) );
                }

                // 更新
                if (UpdateResource(hResource, RT_VERSION, MAKEINTRESOURCE(VS_VERSION_INFO), lpTranslate->wLanguage, lpBuffer, dwSize) != FALSE)
                {
                    if (EndUpdateResource(hResource, FALSE) != FALSE)
                    {
                        bRet = TRUE;
                    }
                }
            }
        }
    }

out:
    if( lpBuffer )
    {
        delete [] lpBuffer;
    }

    return bRet;
}

int CD2ModCenterDlg::readRegIntValue(HKEY hKey, LPCSTR lpSubKey, LPCSTR lpValueName, int defaultValue)
{
    DWORD dwData;
    DWORD cbData;
    HKEY phkResult;

    phkResult = 0;
    cbData = 4;
    dwData = 0;

    if ( RegOpenKeyExA(hKey, lpSubKey, 0, 1u, &phkResult) )
    {
        return defaultValue;
    }

    if ( !RegQueryValueExA(phkResult, lpValueName, 0, 0, (LPBYTE)&dwData, &cbData) )
    {
        RegCloseKey(phkResult);
        return dwData;
    }

    RegCloseKey(phkResult);
    return defaultValue;
}

int CD2ModCenterDlg::writeRegIntValue(HKEY hKey, LPCSTR lpSubKey, LPCSTR lpValueName, DWORD dwType, DWORD Data)
{
    DWORD dwDisposition;
    HKEY phkResult;

    phkResult = 0;
    dwDisposition = 0;

    if ( !RegCreateKeyExA(hKey, lpSubKey, 0, 0, 0, 0xF003Fu, 0, &phkResult, &dwDisposition) )
    {
        if ( !RegSetValueExA(phkResult, lpValueName, 0, dwType, (const BYTE *)&Data, sizeof(Data)) )
        {
            RegCloseKey(phkResult);
            return 0;
        }
        RegCloseKey(phkResult);
    }
    return 1;
}

int CD2ModCenterDlg::writeRegStringValue(HKEY hKey, LPCSTR lpSubKey, LPCSTR lpValueName, BYTE *lpData)
{
    DWORD v4;
    DWORD dwDisposition;
    HKEY phkResult;

    phkResult = 0;
    dwDisposition = 0;
    v4 = strlen((const char *)lpData);
    if ( !RegCreateKeyExA(hKey, lpSubKey, 0, 0, 0, 0xF003Fu, 0, &phkResult, &dwDisposition) )
    {
        if ( !RegSetValueExA(phkResult, lpValueName, 0, 1u, lpData, v4 + 1) )
        {
            RegCloseKey(phkResult);
            return 0;
        }
        RegCloseKey(phkResult);
    }
    return 1;
}

BOOL CD2ModCenterDlg::checkVideoEnv()
{
    if ( readRegIntValue(HKEY_CURRENT_USER, "Software\\Blizzard Entertainment\\Diablo II\\VideoConfig", "Render", 999) == 999 )
    {
        m_strTempLanguage.LoadString(IDS_STRING156);
        ::MessageBox(NULL, m_strTempLanguage, _T(""), MB_OK);
        return FALSE;
    }

    return TRUE;
}

void CD2ModCenterDlg::OnBnClickedBtnstart()
{
    // TODO: 在此添加控件通知处理程序代码
    CStdioFile csFile;
    CFileException cfException;
    CString strModParam;

    DWORD dwHandle = 0;
    DWORD dwSize = 0;

    PROCESS_INFORMATION pi;
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));

    STARTUPINFO si;
    ZeroMemory(&si, sizeof(STARTUPINFO));     //初始化
    si.cb = sizeof(STARTUPINFO);
    si.wShowWindow = SW_SHOW;
    si.dwFlags = STARTF_USESHOWWINDOW;

    wchar_t szCmdline[D2LOADER_MAXPATH] = {0};

    if ( TRUE != checkVideoEnv() )
    {
        return;
    }

    if (m_strModPath.IsEmpty())
    {
        return;
    }

    if ( !m_strVersion.CompareNoCase("Unknown") )
    {
        return;
    }

    //UpdateCmdParam();这里调用会清掉自己手动添加的参数
    GetDlgItemText(IDC_RUNCMD, strModParam);
    CString strExtraParam = strModParam.Right(strModParam.GetLength() - m_strCmdParam.GetLength());

    strModParam.Replace(_T("\r\n"), _T(" "));
    if (csFile.Open(m_strModPath + "\\D2Loader.ini", CFile::typeText | CFile::modeReadWrite | CFile::modeCreate, &cfException))
    {
        CString strRelativeParam = strModParam.Trim();
        strRelativeParam.Replace(m_strWorkingPath, _T("..\\.."));
        char* old_locale = _strdup( setlocale(LC_CTYPE,NULL) );
        setlocale( LC_CTYPE, "chs" );//设定 
        csFile.WriteString(strRelativeParam);
        setlocale( LC_CTYPE, old_locale );
        free( old_locale );//还原区域设定 
        csFile.Close();
    }

    if (!strExtraParam.IsEmpty() && 0 < strExtraParam.Trim().GetLength())
    {
        if (csFile.Open(m_strModPath + "\\D2Loader_Extra.ini", CFile::typeText | CFile::modeReadWrite | CFile::modeCreate, &cfException))
        {
            CString strRelativeParam = strExtraParam.Trim();
            strRelativeParam.Replace(_T("\r\n"), _T("^^&&##"));
            strRelativeParam.Replace(m_strWorkingPath, _T("..\\.."));
            char* old_locale = _strdup(setlocale(LC_CTYPE, NULL));
            setlocale(LC_CTYPE, "chs");//设定 
            csFile.WriteString(strRelativeParam);
            setlocale(LC_CTYPE, old_locale);
            free(old_locale);//还原区域设定 
            csFile.Close();
        }
    }

    if ( !m_strVersion.CompareNoCase("1.14d") && !m_strModType.CompareNoCase(_T("core")) )
    {
        char *apcMpq10[] = {"D2CHAR", "D2data", "D2EXP", "D2music", "D2sfx", "D2SPEECH", "D2VIDEO", "D2xmusic", "D2XTALK", "D2XVIDEO"};
        char temp[MAX_PATH] = { 0 };
        CString strTempPath;

        ((CButton*)GetDlgItem(IDC_BTNSTART))->EnableWindow(FALSE);
        SetCurrentDirectory(m_strModPath);
        for ( DWORD i = 0; i < sizeof(apcMpq10) / sizeof(apcMpq10[0]); i++)
        {
            if ( !PathFileExists(m_strModPath + _T("\\") + apcMpq10[i] + _T(".mpq")) )
            {
                sprintf_s(temp, "mklink %s.mpq %ws\\%s.mpq", apcMpq10[i], m_acWorkingPath, apcMpq10[i]);
                system(temp);
            }
        }
        SetCurrentDirectory(m_strWorkingPath);
        Sleep(5);

        if (!PathIsDirectory(m_strModPath + _T("\\Save")))
        {
            CreateDirectory(m_strModPath + _T("\\Save"), 0);//不存在则创建
        }

        CString strParam = m_strModPath + _T("\\game.exe");
        if ( 0 <= strModParam.Find(_T("-w")) )
        {
            strParam += _T(" -w");
        }
        if ( 0 <= strModParam.Find(_T("-3dfx")) || 0 <= strModParam.Find(_T("-glide")) )
        {
            strParam += _T(" -3dfx");
        }

        CFile gameExe;
        BYTE buf[1024] = { 0 };
        DWORD dwPatchPlugY = 0;
        DWORD dwOpenFileSucc = 0;
        if (gameExe.Open(m_strModPath + _T("\\game.exe"), CFile::modeReadWrite))
        {
            dwOpenFileSucc = 1;

            gameExe.Seek(0x2CBFB0, CFile::begin);
            gameExe.Read(buf, 1024);
            if ( !_stricmp("PlugY.dll", (const char*)buf) )
            {
                dwPatchPlugY = 1;
            }

            gameExe.Seek(0x64C8, CFile::begin);
            gameExe.Read(buf, 1);
            buf[1] = 0;
            if ( 0x90 != *buf )
            {
                buf[0] = 0xE9;  //jmp 006CB5C5;nop
                buf[1] = 0xFD;
                buf[2] = 0x50;
                buf[3] = 0x2C;
                buf[4] = 0x00;
                buf[5] = 0x90;
                gameExe.Seek(0x64C3, CFile::begin);
                gameExe.Write(buf, 6);

                strcpy_s((char *)buf, sizeof(buf), "PatchEntry114d.dll");
                gameExe.Seek(0x2CB5A3, CFile::begin);
                gameExe.Write(buf, strlen((const char *)buf) + 1);

                /*
                pushad
                push 0x6CB5A3   ;dll name
                call dword ptr ds:[<&KERNEL32.LoadLibrar>
                popad
                sub esp,0x4E0
                jmp 004064C9
                */
                buf[0] = 0x60;
                buf[1] = 0x68;
                buf[2] = 0xA3;
                buf[3] = 0xB5;
                buf[4] = 0x6C;
                buf[5] = 0x00;
                buf[6] = 0xFF;
                buf[7] = 0x15;
                buf[8] = 0x44;
                buf[9] = 0xC1;
                buf[10] = 0x6C;
                buf[11] = 0x00;
                buf[12] = 0x61;
                buf[13] = 0x81;
                buf[14] = 0xEC;
                buf[15] = 0xE0;
                buf[16] = 0x04;
                buf[17] = 0x00;
                buf[18] = 0x00;
                buf[19] = 0xE9;
                buf[20] = 0xEC;
                buf[21] = 0xAE;
                buf[22] = 0xD3;
                buf[23] = 0xFF;
                gameExe.Seek(0x2CB5C5, CFile::begin);
                gameExe.Write(buf, 24);
            }

            gameExe.Close();
        }

        if ( 0 <= strModParam.Find(_T("PlugY.dll")) || 0 <= strModParam.Find(_T("PLUGY.dll")) || 0 <= strModParam.Find(_T("plugy.dll")) )
        {
            strParam += _T(" -direct -txt");
            if ( 0 == dwPatchPlugY && 0 != dwOpenFileSucc )
            { 
                SetCurrentDirectory(m_strModPath);
                system("PatchD2File.exe");
                SetCurrentDirectory(m_strWorkingPath);
            }
        }
        else if ( 0 != dwPatchPlugY && 0 != dwOpenFileSucc )
        {
            SetCurrentDirectory(m_strModPath);
            system("RestoreD2File.exe");
            SetCurrentDirectory(m_strWorkingPath);
        }
        wsprintf(szCmdline, strParam);

        WritePrivateProfileString(_T("USERSETTINGS"), _T("ActiveFolder"), m_strModPath, m_strWorkingPath + _T("\\D2ModCenter.ini"));

        SetCurrentDirectory(m_strModPath);
        if (::CreateProcess(NULL, szCmdline, NULL,
            NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi)) {
            if (TRUE == m_boolWaitGameFinish)
            {
                WaitForSingleObject(pi.hProcess, INFINITE);
                CloseHandle(pi.hProcess);
                CloseHandle(pi.hThread);
            }
        }
        SetCurrentDirectory(m_strWorkingPath);
        ((CButton*)GetDlgItem(IDC_BTNSTART))->EnableWindow(TRUE);
        return;
    }

    ((CButton*)GetDlgItem(IDC_BTNSTART))->EnableWindow(FALSE);
    UpdateD2LoaderVersion(_T("D2Loader.exe"));
    if (PathFileExists(_T("game.exe")))
    {
        UpdateD2LoaderVersion(_T("game.exe"));
    }

    if ( PathFileExists(_T("Tools\\PlugY\\PlugY.ini")) && 1 == ((CButton*)GetDlgItem(IDC_CHECKPLUGY))->GetCheck() )
    {
        //存在则不覆盖
        CopyFile(_T("Tools\\PlugY\\PlugY.ini"), m_strModPath + "\\PlugY.ini", TRUE);
    }

#if 0
    //BaseMod不需要复制ini，他是在dll目录下找ini的
    if ( PathFileExists(_T("Tools\\BaseMod\\BaseMod.mpq")))
    {
        //存在则不覆盖
        CopyFile(_T("Tools\\BaseMod\\BaseMod.mpq"), m_strModPath + "\\BaseMod.mpq", TRUE);
    }
#endif

    CString strTemp;
    m_cbVideoMode.GetLBText(m_cbVideoMode.GetCurSel(), strTemp);

    if ( !strTemp.CompareNoCase(_T("CNC-DDraw")) && PathFileExists(_T("Tools\\cnc-ddraw\\ddraw.ini"))  )
    {
        //存在则不覆盖
        CopyFile(_T("Tools\\cnc-ddraw\\ddraw.ini"), m_strModPath + "\\ddraw.ini", TRUE);
    }

    if ( !strTemp.CompareNoCase(_T("D2DX")) && PathFileExists(_T("Tools\\d2dx\\d2dx.cfg")) )
    {
        //存在则不覆盖
        CopyFile(_T("Tools\\d2dx\\d2dx.cfg"), m_strModPath + "\\d2dx.cfg", TRUE);
        if (PathFileExists(_T("Tools\\gl32ogl14e\\glide3x.dll")))
        {
            //存在则不覆盖，因为0.99.510以后的d2dx需要当前运行目录下有glide3x.dll才能正常运行，虽然不用，但必须有，坑爹逻辑
            CopyFile(_T("Tools\\gl32ogl14e\\glide3x.dll"), m_strModPath + "\\glide3x.dll", TRUE);
        }
    }

    if ( !strTemp.CompareNoCase(_T("D2DX510")) && PathFileExists(_T("Tools\\d2dx510\\d2dx.cfg")) )
    {
        //存在则不覆盖
        CopyFile(_T("Tools\\d2dx510\\d2dx.cfg"), m_strModPath + "\\d2dx.cfg", TRUE);
    }

#if 1
    wsprintf(szCmdline, _T("D2Loader.exe %s"), strModParam);

    WritePrivateProfileString(_T("USERSETTINGS"), _T("ActiveFolder"), m_strModPath, m_strWorkingPath + _T("\\D2ModCenter.ini"));
    if (::CreateProcess(NULL, szCmdline, NULL,
        NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, m_strModPath, &si, &pi)) {
        if (TRUE == m_boolWaitGameFinish)
        {
            WaitForSingleObject(pi.hProcess, INFINITE);
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
        }
    }
    ((CButton*)GetDlgItem(IDC_BTNSTART))->EnableWindow(TRUE);
#else
    dwSize = GetFileVersionInfoSize(m_strModPath + _T("\\D2Loader.exe"), &dwHandle);
    if ( 0 >= dwSize)
    {
        DeleteFile(m_strModPath + _T("\\D2Loader.exe"));
    }

    if (!PathFileExists(m_strModPath + _T("\\D2Loader.exe")))
    {
        char temp[MAX_PATH] = { 0 };
        SetCurrentDirectory(m_strModPath);
        sprintf_s(temp, "mklink D2Loader.exe %ws\\D2Loader.exe", m_acWorkingPath);
        system(temp);
        SetCurrentDirectory(m_strWorkingPath);
        Sleep(5);
    }

    wsprintf(szCmdline, _T("%s\\D2Loader.exe %s"), m_strModPath, strModParam);
    if (::CreateProcess(NULL, szCmdline, NULL,
        NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi)) {
    }
#endif
}

void CD2ModCenterDlg::OnOK()
{
    // TODO: 在此添加专用代码和/或调用基类
    OnBnClickedBtnstart();
    //CDialogEx::OnOK();
}

void CD2ModCenterDlg::OnNMRClickListmod(NMHDR* pNMHDR, LRESULT* pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    // TODO: 在此添加控件通知处理程序代码
    *pResult = 0;
    CMenu menu;
    POINT pt = { 0 };
    GetCursorPos(&pt);//得到鼠标点击位置
    menu.LoadMenu(IDR_MENU1);//菜单资源ID
    menu.GetSubMenu(0)->TrackPopupMenu(0, pt.x, pt.y, this);
}

void CD2ModCenterDlg::OnClistmenu32771()
{
    // TODO: 在此添加命令处理程序代码
    if (PathFileExists(m_strModPath + "\\D2Loader.ini"))
    {
        DeleteFile(m_strModPath + "\\D2Loader.ini");
    }
    if (PathFileExists(m_strModPath + "\\D2Loader_Extra.ini"))
    {
        DeleteFile(m_strModPath + "\\D2Loader_Extra.ini");
    }

    for ( DWORD i =0; i < m_dwModCount; i++ )
    {
        CString strModPath = m_ModList.GetItemText(i, 2);
        if ( !strModPath.CompareNoCase(m_strModPath) )
        {
            m_ModList.SetItemState(i, LVNI_FOCUSED | LVIS_SELECTED, LVNI_FOCUSED | LVIS_SELECTED);
            SelectOneMod(i);
            break;
        }
    }
}

static BOOL sub_40B050()
{
  HANDLE v0; // eax
  HANDLE v1; // eax
  SIZE_T v2; // ebx
  HLOCAL pSecurityDescriptor; // [esp+Ch] [ebp-64h]
  ACL *pAcl; // [esp+10h] [ebp-60h]
  struct _PRIVILEGE_SET PrivilegeSet; // [esp+14h] [ebp-5Ch]
  GENERIC_MAPPING GenericMapping; // [esp+28h] [ebp-48h]
  struct _SID_IDENTIFIER_AUTHORITY pIdentifierAuthority; // [esp+38h] [ebp-38h]
  HANDLE DuplicateTokenHandle; // [esp+40h] [ebp-30h]
  HANDLE TokenHandle; // [esp+44h] [ebp-2Ch]
  PSID pSid; // [esp+48h] [ebp-28h]
  DWORD PrivilegeSetLength; // [esp+4Ch] [ebp-24h]
  DWORD GrantedAccess; // [esp+50h] [ebp-20h]
  BOOL AccessStatus; // [esp+54h] [ebp-1Ch]
  //CPPEH_RECORD ms_exc; // [esp+58h] [ebp-18h]

  AccessStatus = 0;
  PrivilegeSetLength = 20;
  pSid = 0;
  TokenHandle = 0;
  DuplicateTokenHandle = 0;
  *(DWORD *)pIdentifierAuthority.Value = 0;//unk_418E30;
  *(WORD *)&pIdentifierAuthority.Value[4] = 0x0500;//*((WORD *)&unk_418E30 + 2);
  //ms_exc.registration.TryLevel = 0;
  v0 = GetCurrentThread();
  if ( OpenThreadToken(v0, 0xAu, 1, &TokenHandle)
    || (pAcl = 0, pSecurityDescriptor = 0, GetLastError() == 1008)
    && (v1 = GetCurrentProcess(), pAcl = 0, pSecurityDescriptor = 0, OpenProcessToken(v1, 0xAu, &TokenHandle)) )
  {
    pAcl = 0;
    pSecurityDescriptor = 0;
    if ( DuplicateToken(TokenHandle, SecurityImpersonation, &DuplicateTokenHandle) )
    {
      pAcl = 0;
      pSecurityDescriptor = 0;
      if ( AllocateAndInitializeSid(&pIdentifierAuthority, 2u, 0x20u, 0x220u, 0, 0, 0, 0, 0, 0, &pSid) )
      {
        pSecurityDescriptor = LocalAlloc(0x40u, 0x14u);
        pAcl = 0;
        if ( pSecurityDescriptor )
        {
          pAcl = 0;
          if ( InitializeSecurityDescriptor(pSecurityDescriptor, 1u) )
          {
            v2 = GetLengthSid(pSid) + 16;
            pAcl = (ACL *)LocalAlloc(0x40u, v2);
            if ( pAcl )
            {
              if ( InitializeAcl(pAcl, v2, 2u) )
              {
                if ( AddAccessAllowedAce(pAcl, 2u, 3u, pSid) )
                {
                  if ( SetSecurityDescriptorDacl(pSecurityDescriptor, 1, pAcl, 0) )
                  {
                    SetSecurityDescriptorGroup(pSecurityDescriptor, pSid, 0);
                    SetSecurityDescriptorOwner(pSecurityDescriptor, pSid, 0);
                    if ( IsValidSecurityDescriptor(pSecurityDescriptor) )
                    {
                      GenericMapping.GenericRead = 1;
                      GenericMapping.GenericWrite = 2;
                      GenericMapping.GenericExecute = 0;
                      GenericMapping.GenericAll = 3;
                      if ( !AccessCheck(
                              pSecurityDescriptor,
                              DuplicateTokenHandle,
                              1u,
                              &GenericMapping,
                              &PrivilegeSet,
                              &PrivilegeSetLength,
                              &GrantedAccess,
                              &AccessStatus) )
                        AccessStatus = 0;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  //ms_exc.registration.TryLevel = -1;
  if ( pAcl )
    LocalFree(pAcl);
  if ( pSecurityDescriptor )
    LocalFree(pSecurityDescriptor);
  if ( pSid )
    FreeSid(pSid);
  if ( DuplicateTokenHandle )
    CloseHandle(DuplicateTokenHandle);
  if ( TokenHandle )
    CloseHandle(TokenHandle);
  return AccessStatus;
}

void CD2ModCenterDlg::OnBnClickedBtnd2vidtst()
{
    // TODO: 在此添加控件通知处理程序代码
    PROCESS_INFORMATION pi;
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));

    STARTUPINFO si;
    ZeroMemory(&si, sizeof(STARTUPINFO));     //初始化
    si.cb = sizeof(STARTUPINFO);
    si.wShowWindow = SW_SHOW;
    si.dwFlags = STARTF_USESHOWWINDOW;

    if ( !::sub_40B050() )
    {
        m_strTempLanguage.LoadString(IDS_STRING148);
        ::MessageBox(NULL, m_strTempLanguage, _T(""), MB_OK);
        return;
    }

    CopyFile(_T("CORES\\1.13c\\Fog.dll"), _T("Fog.dll"), FALSE);
    CopyFile(_T("CORES\\1.13c\\Storm.dll"), _T("Storm.dll"), FALSE);
    CopyFile(_T("Tools\\gl32ogl14e\\glide3x.dll"), _T("glide3x.dll"), FALSE);

    wchar_t szCmdline[D2LOADER_MAXPATH] = { 0 };
    wsprintf(szCmdline, _T("D2VidTst.exe"));

    if (::CreateProcess(NULL, szCmdline, NULL,
        NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi)) {
        WaitForSingleObject(pi.hProcess, INFINITE);
        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }

    wsprintf(szCmdline, _T("glide-init.exe"));

    if (::CreateProcess(NULL, szCmdline, NULL,
        NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi)) {
        WaitForSingleObject(pi.hProcess, INFINITE);
        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }
}

BOOL CD2ModCenterDlg::CopyDirectory(CString strSrcPath, CString strDestPath)
{
    CFileFind m_sFileFind;
    if (strSrcPath.IsEmpty())
    {
        return FALSE;
    }
    if (!m_sFileFind.FindFile(strDestPath))
    {
        CreateDirectory(strDestPath,NULL);//创建目标文件夹
    }
    CFileFind finder;
    CString path;
    path.Format(_T("%s/*.*"),strSrcPath);
    BOOL bWorking = finder.FindFile(path);
    while (bWorking)
    {
        bWorking = finder.FindNextFile();
        if (finder.IsDirectory() && !finder.IsDots())//是文件夹 而且 名称不含 . 或 ..
        {
            CopyDirectory(finder.GetFilePath(), strDestPath + "/"+ finder.GetFileName());//递归创建文件夹+"/"+finder.GetFileName()
        }
        else
        {//是文件，则直接复制
            CopyFile(finder.GetFilePath(), strDestPath+"/"+finder.GetFileName(), FALSE);
        }
    }

    return TRUE;
}

BOOL CD2ModCenterDlg::DeleteFolder(LPCTSTR lpszPath)
{
    int nLength = wcslen(lpszPath);
    TCHAR *NewPath = new TCHAR[nLength + 2];
    wcscpy_s(NewPath, nLength + 2, lpszPath);
    NewPath[nLength] = '\0';
    NewPath[nLength + 1] = '\0';
    SHFILEOPSTRUCT FileOp;
    ZeroMemory((void*)&FileOp,sizeof(SHFILEOPSTRUCT));
    FileOp.fFlags = FOF_NOCONFIRMATION;
    FileOp.hNameMappings = NULL;
    FileOp.hwnd = NULL;
    FileOp.lpszProgressTitle = NULL;
    FileOp.pFrom = NewPath;
    FileOp.pTo = NULL;
    FileOp.wFunc = FO_DELETE;
    SHFileOperation(&FileOp);
    delete NewPath;
    return TRUE;
    //return SHFileOperation(&FileOp) == 0;
}

void CD2ModCenterDlg::OnBnClickedCheckcopyd2hd()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtnhackpre()
{
    // TODO: 在此添加控件通知处理程序代码
    CString gReadFilePathName;
    CFileDialog fileDlg(true, _T("*"), _T("*.*"), OFN_OVERWRITEPROMPT, _T("all Files (*.*)"), NULL);
    fileDlg.m_ofn.lpstrInitialDir = m_strModPath;
    if (fileDlg.DoModal() == IDOK)    //弹出对话框
    {
        gReadFilePathName = fileDlg.GetPathName();//得到完整的文件名和目录名拓展名
        //CString filename = fileDlg.GetFileName();
        SetDlgItemText(IDC_HACKPRE, gReadFilePathName);
    }

    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedBtnhack()
{
    // TODO: 在此添加控件通知处理程序代码
    CString gReadFilePathName;
    CFileDialog fileDlg(true, _T("*"), _T("*.*"), OFN_OVERWRITEPROMPT, _T("all Files (*.*)"), NULL);
    fileDlg.m_ofn.lpstrInitialDir = m_strModPath;
    if (fileDlg.DoModal() == IDOK)    //弹出对话框
    {
        gReadFilePathName = fileDlg.GetPathName();//得到完整的文件名和目录名拓展名
        //CString filename = fileDlg.GetFileName();
        SetDlgItemText(IDC_HACK, gReadFilePathName);
    }

    UpdateCmdParam();
}

bool CD2ModCenterDlg::EnableDebugPrivilege()
{
    HANDLE hToken;
    LUID sedebugnameValue;
    TOKEN_PRIVILEGES tkp;
    if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
    {
        return   FALSE;
    }
    if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &sedebugnameValue))
    {
        CloseHandle(hToken);
        return false;
    }
    tkp.PrivilegeCount = 1;
    tkp.Privileges[0].Luid = sedebugnameValue;
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    if (!AdjustTokenPrivileges(hToken, FALSE, &tkp, sizeof(tkp), NULL, NULL))
    {
        CloseHandle(hToken);
        return false;
    }
    return true;
}

void CD2ModCenterDlg::CloseProcess(CString strExeName)
{
    EnableDebugPrivilege();
    int     rc = 0;
    HANDLE  hSysSnapshot = NULL;
    PROCESSENTRY32 proc;
    hSysSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (hSysSnapshot == (HANDLE)-1)   return;
    proc.dwSize = sizeof(proc);
    if (Process32First(hSysSnapshot, &proc))
    {
        do {
            if ( !strExeName.CompareNoCase(proc.szExeFile) )
            {
                HANDLE Proc_handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, proc.th32ProcessID);
                if (Proc_handle == NULL || !TerminateProcess(Proc_handle, 0)) rc = 1;
                else rc = 0;
            }
        } while (Process32Next(hSysSnapshot, &proc));
    }

    CloseHandle(hSysSnapshot);

    return;
}

void CD2ModCenterDlg::OnBnClickedBtnkilld2()
{
    // TODO: 在此添加控件通知处理程序代码
    if ( !::sub_40B050() )
    {
        m_strTempLanguage.LoadString(IDS_STRING148);
        ::MessageBox(NULL, m_strTempLanguage, _T(""), MB_OK);
        return;
    }

    CloseProcess(_T("D2VidTst.exe"));
    CloseProcess(_T("D2VidTst.exe"));
    CloseProcess(_T("D2VidTst.exe"));

    CloseProcess(_T("glide-init.exe"));
    CloseProcess(_T("glide-init.exe"));
    CloseProcess(_T("glide-init.exe"));

    CloseProcess(_T("D2Loader.exe"));
    CloseProcess(_T("D2Loader.exe"));
    CloseProcess(_T("D2Loader.exe"));
    CloseProcess(_T("D2Loader.exe"));
    CloseProcess(_T("D2Loader.exe"));
}

void CD2ModCenterDlg::PatchBaseMod1133(CString strFileName)
{
    CFile gameExe;
    BYTE acLoaderName[] = {"D2Loader.exe"};
    BYTE buf[5] = { 0 };

    if (gameExe.Open(strFileName, CFile::modeReadWrite))
    {
        gameExe.Seek(0x9D19, CFile::begin);
        gameExe.Read(buf, sizeof(buf));

        if ( 0x68 == buf[0] && 0x10 == buf[1] && 0xF0 == buf[2] && 0x01 == buf[3] && 0x10 == buf[4] )
        {
            if ( IDYES == ::MessageBox(NULL, _T("是否修改") + strFileName + _T("(1133)支持本程序?"), _T("提示"), MB_YESNO | MB_ICONQUESTION) )
            {
                gameExe.Seek(0x1CFF3, CFile::begin);
                gameExe.Write(acLoaderName, strlen((const char*)acLoaderName));

                buf[1] = 0xF3;
                buf[2] = 0xCF;
                buf[3] = 0x01;
                buf[4] = 0x10;
                gameExe.Seek(0x9D19, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0x9D73, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0x16860, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                ::MessageBox(NULL, strFileName + _T("(1133)已修改!"),   _T("提示"), MB_OK);
            }
        }
        else  if ( 0x68 == buf[0] && 0xF3 == buf[1] && 0xCF == buf[2] && 0x01 == buf[3] && 0x10 == buf[4] )
        {
            if ( IDYES == ::MessageBox(NULL, _T("是否还原") + strFileName + _T("(1133)?"), _T("提示"), MB_YESNO | MB_ICONQUESTION) )
            {
                memset(acLoaderName, 0, sizeof(acLoaderName));
                gameExe.Seek(0x1CFF3, CFile::begin);
                gameExe.Write(acLoaderName, sizeof(acLoaderName));

                buf[1] = 0x10;
                buf[2] = 0xF0;
                buf[3] = 0x01;
                buf[4] = 0x10;
                gameExe.Seek(0x9D19, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0x9D73, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0x16860, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                ::MessageBox(NULL, _T("") + strFileName + _T("(1133)已还原!"), _T("提示"), MB_OK);
            }
        }
        else
        {
            ::MessageBox(NULL, strFileName + _T("(1133)非原版，无法处理!"), _T("提示"), MB_OK);
        }

        gameExe.Close();
    }
}

void CD2ModCenterDlg::PatchBaseMod1135(CString strFileName)
{
    CFile gameExe;
    BYTE acLoaderName[] = {"D2Loader.exe"};
    BYTE buf[5] = { 0 };

    if (gameExe.Open(strFileName, CFile::modeReadWrite))
    {
        gameExe.Seek(0xB8C6, CFile::begin);
        gameExe.Read(buf, sizeof(buf));

        if ( 0x68 == buf[0] && 0x58 == buf[1] && 0x70 == buf[2] && 0x02 == buf[3] && 0x10 == buf[4] )
        {
            if ( IDYES == ::MessageBox(NULL, _T("是否修改") + strFileName + _T("(1135)支持本程序?"), _T("提示"), MB_YESNO | MB_ICONQUESTION) )
            {
                gameExe.Seek(0x24FF3, CFile::begin);
                gameExe.Write(acLoaderName, strlen((const char*)acLoaderName));

                buf[1] = 0xF3;
                buf[2] = 0x4F;
                buf[3] = 0x02;
                buf[4] = 0x10;
                gameExe.Seek(0xB8C6, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0xB933, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0x1E1B0, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                ::MessageBox(NULL, strFileName + _T("(1135)已修改!"),   _T("提示"), MB_OK);
            }
        }
        else  if ( 0x68 == buf[0] && 0xF3 == buf[1] && 0x4F == buf[2] && 0x02 == buf[3] && 0x10 == buf[4] )
        {
            if ( IDYES == ::MessageBox(NULL, _T("是否还原") + strFileName + _T("(1135)?"), _T("提示"), MB_YESNO | MB_ICONQUESTION) )
            {
                memset(acLoaderName, 0, sizeof(acLoaderName));
                gameExe.Seek(0x24FF3, CFile::begin);
                gameExe.Write(acLoaderName, sizeof(acLoaderName));

                buf[1] = 0x58;
                buf[2] = 0x70;
                buf[3] = 0x02;
                buf[4] = 0x10;
                gameExe.Seek(0xB8C6, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0xB933, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0x1E1B0, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                ::MessageBox(NULL, _T("") + strFileName + _T("(1135)已还原!"), _T("提示"), MB_OK);
            }
        }
        else
        {
            ::MessageBox(NULL, strFileName + _T("(1135)非原版，无法处理!"), _T("提示"), MB_OK);
        }

        gameExe.Close();
    }
}

void CD2ModCenterDlg::PatchBaseMod1136(CString strFileName)
{
    CFile gameExe;
    BYTE acLoaderName[] = {"D2Loader.exe"};
    BYTE buf[5] = { 0 };

    if (gameExe.Open(strFileName, CFile::modeReadWrite))
    {
        gameExe.Seek(0xF296, CFile::begin);
        gameExe.Read(buf, sizeof(buf));

        if ( 0x68 == buf[0] && 0x40 == buf[1] && 0xF9 == buf[2] && 0x02 == buf[3] && 0x10 == buf[4] )
        {
            if ( IDYES == ::MessageBox(NULL, _T("是否修改") + strFileName + _T("(1136)支持本程序?"), _T("提示"), MB_YESNO | MB_ICONQUESTION) )
            {
                gameExe.Seek(0x2CFF3, CFile::begin);
                gameExe.Write(acLoaderName, strlen((const char*)acLoaderName));

                buf[1] = 0xF3;
                buf[2] = 0xCF;
                buf[3] = 0x02;
                buf[4] = 0x10;
                gameExe.Seek(0xF296, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0xF313, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0x236D0, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                ::MessageBox(NULL, strFileName + _T("(1136)已修改!"),   _T("提示"), MB_OK);
            }
        }
        else  if ( 0x68 == buf[0] && 0xF3 == buf[1] && 0xCF == buf[2] && 0x02 == buf[3] && 0x10 == buf[4] )
        {
            if ( IDYES == ::MessageBox(NULL, _T("是否还原") + strFileName + _T("(1136)?"), _T("提示"), MB_YESNO | MB_ICONQUESTION) )
            {
                memset(acLoaderName, 0, sizeof(acLoaderName));
                gameExe.Seek(0x2CFF3, CFile::begin);
                gameExe.Write(acLoaderName, sizeof(acLoaderName));

                buf[1] = 0x40;
                buf[2] = 0xF9;
                buf[3] = 0x02;
                buf[4] = 0x10;
                gameExe.Seek(0xF296, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0xF313, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                gameExe.Seek(0x236D0, CFile::begin);
                gameExe.Write(buf, sizeof(buf));
                ::MessageBox(NULL, _T("") + strFileName + _T("(1136)已还原!"), _T("提示"), MB_OK);
            }
        }
        else
        {
            ::MessageBox(NULL, strFileName + _T("(1136)非原版，无法处理!"), _T("提示"), MB_OK);
        }

        gameExe.Close();
    }
}

void CD2ModCenterDlg::PatchBaseModEntry(CString strFileName)
{
    // TODO: 在此添加控件通知处理程序代码
    if ( !PathFileExists(strFileName) )
    {
        return;
    }

    DWORD dwHandle = 0;
    DWORD dwSize = 0;

    struct  
    {  
        WORD wLanguage;  
        WORD wCodePage;  
    } *lpTranslate;

    dwSize = GetFileVersionInfoSize(strFileName, &dwHandle);
    if ( 0 >= dwSize)
    {
        return;
    }

    LPBYTE lpBuffer = new BYTE[dwSize];
    memset( lpBuffer, 0, dwSize );

    if (GetFileVersionInfo(strFileName, dwHandle, dwSize, lpBuffer) != FALSE)
    {
        HANDLE hResource = BeginUpdateResource(strFileName, FALSE);
        if (NULL != hResource)
        {
            UINT uTemp;
            DWORD dwVer[4] = {0};

            if (VerQueryValue(lpBuffer, _T("\\VarFileInfo\\Translation"), (LPVOID *)&lpTranslate, &uTemp) != FALSE)
            {
                LPVOID lpFixedBuf = NULL;
                DWORD dwFixedLen = 0;
                if( FALSE != VerQueryValue( lpBuffer, _T("\\"), &lpFixedBuf, (PUINT)&dwFixedLen ))
                {
                    VS_FIXEDFILEINFO* pFixedInfo = (VS_FIXEDFILEINFO*)lpFixedBuf;

                    if ( pFixedInfo->dwFileVersionMS == 0x10001 && pFixedInfo->dwFileVersionLS == 0x30003 )
                    {
                        PatchBaseMod1133(strFileName);
                    }
                    else if ( pFixedInfo->dwFileVersionMS == 0x10001 && pFixedInfo->dwFileVersionLS == 0x30005 )
                    {
                        PatchBaseMod1135(strFileName);
                    }
                    else if ( pFixedInfo->dwFileVersionMS == 0x10001 && pFixedInfo->dwFileVersionLS == 0x30006 )
                    {
                        PatchBaseMod1136(strFileName);
                    }
                }
            }
        }
    }

    if( lpBuffer )
    {
        delete [] lpBuffer;
    }

    return;
}

void CD2ModCenterDlg::OnBnClickedBtnbasemod()
{
    // TODO: 在此添加控件通知处理程序代码
    //PatchBaseModEntry(_T("Tools\\BaseMod\\BaseMod.dll"));

    if ( m_strModPath.IsEmpty() )
    {
        return;
    }
    PatchBaseModEntry(m_strModPath + _T("\\BaseMod.dll"));
}

void CAboutDlg::OnBnClickedChecktest()
{
    // TODO: 在此添加控件通知处理程序代码
    if (1 == ((CButton*)GetDlgItem(IDC_CHECKTEST))->GetCheck())
    {
        m_boolTest = TRUE;
    }
    else
    {
        m_boolTest = FALSE;
    }
}

void CD2ModCenterDlg::OnTimer(UINT_PTR nIDEvent)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    CStdioFile csFile;
    CFileException cfException;
    
    if (1 == nIDEvent)
    {
        if (PathFileExists(_T("test_target.txt")))
        {
            CString strReadData;

            if (csFile.Open(_T("test_target.txt"), CFile::typeText | CFile::modeRead | CFile::modeNoTruncate, &cfException))//以txt方式读取|文件打开时不清除
            {
                char* old_locale = _strdup( setlocale(LC_CTYPE, NULL) );
                setlocale( LC_CTYPE, "chs" );//设定 
                csFile.ReadString(strReadData);
                setlocale( LC_CTYPE, old_locale );
                free( old_locale );//还原区域设定 
                csFile.Close();
                DeleteFile(_T("test_target.txt"));

                for ( DWORD i =0; i < m_dwModCount; i++ )
                {
                    CString strModPath = m_ModList.GetItemText(i, 2);
                    if ( !strModPath.CompareNoCase(m_strWorkingPath + _T("\\") + strReadData) )
                    {
                        m_ModList.SetItemState(i, LVNI_FOCUSED | LVIS_SELECTED, LVNI_FOCUSED | LVIS_SELECTED);
                        SelectOneMod(i);
                        SetTimer(2, 5000, NULL);
                        break;
                    }
                }
            }
        }
    }
    else if (2 == nIDEvent)
    {
        KillTimer(2);
        m_boolWaitGameFinish = TRUE;
        OnBnClickedBtnstart();
        m_boolWaitGameFinish = FALSE;
        OnBnClickedBtnkilld2();
        if (csFile.Open(m_strModPath + "\\test_result.txt", CFile::typeText | CFile::modeReadWrite | CFile::modeCreate, &cfException))//以txt方式读取|文件打开时不清除
        {
            char* old_locale = _strdup(setlocale(LC_CTYPE, NULL));
            setlocale(LC_CTYPE, "chs");//设定 
            csFile.WriteString(_T("Finish！"));
            setlocale(LC_CTYPE, old_locale);
            free(old_locale);//还原区域设定 
            csFile.Close();
        }
    }
    else if (3 == nIDEvent)
    {
        if ( m_astrPrompt[m_dwCurrentPrompt].IsEmpty() )
        {
            m_dwCurrentPrompt = 0;
        }
        SetDlgItemText(IDC_PROMPT, m_astrPrompt[m_dwCurrentPrompt]);
        m_dwCurrentPrompt++;
        m_dwCurrentPrompt %= MAX_PROMPT_INFO;
    }

    CDialogEx::OnTimer(nIDEvent);
}

void CAboutDlg::OnCbnSelchangeSoftwarelocale()
{
    // TODO: 在此添加控件通知处理程序代码
    m_dwCurrentLocale = m_cbSoftLocale.GetCurSel();
}

void CD2ModCenterDlg::OnClose()
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    CDialogEx::OnClose();
    if (m_bRestartFlag)
    {
        CString strFileName = _T("");
        GetModuleFileName(NULL, strFileName.GetBuffer(MAX_PATH), MAX_PATH);
        ShellExecute(NULL, _T(""), strFileName, NULL, NULL, SW_SHOWNORMAL);
        strFileName.ReleaseBuffer();
    }
}

void CD2ModCenterDlg::setLocale(DWORD dwLocale)
{
    m_dwCurrentLocale = dwLocale;
}

void CD2ModCenterDlg::OnBnClickedCheckrestoredll()
{
    // TODO: 在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckcommondll()
{
    // TODO: 在此添加控件通知处理程序代码在此添加控件通知处理程序代码
    UpdateCmdParam();
}

void CD2ModCenterDlg::OnBnClickedCheckalldll()
{
    // TODO: 在此添加控件通知处理程序代码在此添加控件通知处理程序代码
    UpdateCmdParam();
}
